import { Skeleton as MuiSkeleton } from '@mui/material';

export const Skeleton = ({ loading, children, ...props }) =>
  loading ? <MuiSkeleton children={children} width="100%" height="100%" {...props} /> : children;
