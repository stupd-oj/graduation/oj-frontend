import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Label from '@components/Label';

BadgeItem.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  sx: PropTypes.object,
};

export default function BadgeItem({ name, type, sx }) {
  const theme = useTheme();
  const isLight = theme.palette.mode === 'light';
  return (
    <Label variant={isLight ? 'ghost' : 'filled'} color={type || 'success'} sx={{ m: 1, ...sx }}>
      {name}
    </Label>
  );
}
