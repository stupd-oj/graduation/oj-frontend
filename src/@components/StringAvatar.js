import PropTypes from 'prop-types';
import { Avatar, useTheme } from '@mui/material';

const StringAvatar = ({ color = 'default', sx, children, ...other }) => {
  const theme = useTheme();

  if (color === 'default') {
    return (
      <Avatar sx={sx} {...other}>
        {children}
      </Avatar>
    );
  }

  return (
    <Avatar
      sx={{
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette[color].contrastText,
        backgroundColor: theme.palette[color].main,
        ...sx,
      }}
      {...other}
    >
      {children}
    </Avatar>
  );
};

StringAvatar.propTypes = {
  children: PropTypes.node,
  sx: PropTypes.object,
  color: PropTypes.oneOf(['default', 'primary', 'secondary', 'info', 'success', 'warning', 'error']),
};

export default StringAvatar;
