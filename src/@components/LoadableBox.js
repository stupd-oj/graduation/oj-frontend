import { Box, CircularProgress, colors } from '@mui/material';

export default function LoadableBox({ loading, children, ...other }) {
  return (
    <Box {...other} position="relative">
      {loading && (
        <Box
          position="absolute"
          display="flex"
          justifyContent="center"
          alignItems="center"
          sx={{ top: 0, bottom: 0, right: 0, left: 0, backgroundColor: `${colors.grey['300']}50`, zIndex: 999 }}
        >
          <CircularProgress />
        </Box>
      )}
      {children}
    </Box>
  );
}
