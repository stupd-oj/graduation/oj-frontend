import { userValidation } from 'contants/validation';
import { Form, FormikProvider, useFormik } from 'formik';
import * as Yup from 'yup';
import useAuth from 'hooks/useAuth';
import {
  Box,
  Card,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Stack,
  TextField,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useToast } from 'hooks';
import { UserService } from 'services';

export default function AccountUpdateForm() {
  const [{ user }, { updateUser }] = useAuth();
  const { showError, showSuccess } = useToast();
  const initialValues = {
    username: user?.username ?? '',
    fullName: user?.fullName ?? '',
    schoolName: user?.schoolName ?? '',
    className: user?.className ?? '',
    studentId: user?.studentId ?? '',
    mood: user?.mood ?? '',
    email: user?.email ?? '',
    phoneNumber: user?.phoneNumber ?? '',
    gender: user?.gender ?? null,
  };

  const validationSchema = Yup.object({
    fullName: userValidation.fullName,
    phoneNumber: userValidation.phoneNumber,
  });

  const onSubmit = async (values, { setSubmitting }) => {
    try {
      const payload = { ...values };
      payload.gender = values.gender === 'male';
      await UserService.updateUserProfile(payload);
      updateUser(payload);
      showSuccess('Profile updated successfully');
    } catch (e) {
      showError(e.message || 'Update error');
    }
    setSubmitting(false);
  };
  const formik = useFormik({ initialValues, validationSchema, onSubmit });
  const { values, errors, touched, isSubmitting, handleSubmit, getFieldProps, setFieldValue, isValid } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Card sx={{ p: 3 }}>
          <Stack spacing={{ xs: 3, md: 3 }}>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField fullWidth label="Username" {...getFieldProps('username')} disabled />
              <TextField fullWidth label="Email" {...getFieldProps('email')} disabled />
            </Stack>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField
                fullWidth
                label="Full name"
                {...getFieldProps('fullName')}
                error={touched.fullName && !!errors.fullName}
                helperText={touched.fullName && errors.fullName}
              />
              <TextField
                fullWidth
                label="Phone number"
                {...getFieldProps('phoneNumber')}
                error={touched.phoneNumber && !!errors.phoneNumber}
                helperText={touched.phoneNumber && errors.phoneNumber}
              />
            </Stack>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <FormControl fullWidth component="fieldset">
                <FormLabel component="legend">Gender</FormLabel>
                <RadioGroup
                  {...getFieldProps('gender')}
                  row
                  aria-label="Gender"
                  name="gender"
                  onChange={(e) => {
                    setFieldValue('gender', e?.target?.value === 'female');
                  }}
                >
                  <FormControlLabel
                    value="male"
                    control={<Radio />}
                    label="Male"
                    checked={values.gender !== null && !values.gender}
                  />
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="Female"
                    checked={values.gender !== null && values.gender}
                  />
                </RadioGroup>
              </FormControl>
            </Stack>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField
                fullWidth
                label="School"
                {...getFieldProps('schoolName')}
                error={touched.schoolName && !!errors.schoolName}
                helperText={touched.schoolName && errors.schoolName}
              />
            </Stack>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField
                fullWidth
                label="Classname"
                {...getFieldProps('className')}
                error={touched.className && !!errors.className}
                helperText={touched.className && errors.className}
              />
              <TextField
                fullWidth
                label="Student ID"
                {...getFieldProps('studentId')}
                error={touched.studentId && !!errors.studentId}
                helperText={touched.studentId && errors.studentId}
              />
            </Stack>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField
                fullWidth
                label="Mood"
                {...getFieldProps('mood')}
                error={touched.mood && !!errors.mood}
                helperText={touched.mood && errors.mood}
              />
            </Stack>
          </Stack>
          <Box sx={{ mt: 3, display: 'flex', justifyContent: 'flex-end' }}>
            <LoadingButton type="submit" variant="contained" loading={isSubmitting} disabled={!isValid}>
              Save Changes
            </LoadingButton>
          </Box>
        </Card>
      </Form>
    </FormikProvider>
  );
}
