import { userValidation } from 'contants/validation';
import { Form, FormikProvider, useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Card, Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useToast } from 'hooks';
import { UserService } from 'services';

export default function AccountChangePassword() {
  const { showError, showSuccess } = useToast();
  const initialValues = {
    oldPassword: '',
    newPassword: '',
    repeatNewPassword: '',
  };

  const validationSchema = Yup.object({
    newPassword: userValidation.password,
    repeatNewPassword: userValidation.confirmNewPassword,
  });

  const onSubmit = async (values, { setSubmitting }) => {
    try {
      await UserService.changePassword(values);
      showSuccess('Update password successfully');
    } catch (e) {
      setSubmitting(false);
      showError(e.message);
    } finally {
      setSubmitting(false);
    }
  };
  const formik = useFormik({ initialValues, validationSchema, onSubmit });
  const { errors, touched, isSubmitting, handleSubmit, getFieldProps, isValid } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Card sx={{ p: 3 }}>
          <Stack spacing={{ xs: 3, md: 3 }}>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField
                type="password"
                fullWidth
                label="Old password"
                {...getFieldProps('oldPassword')}
                error={touched.oldPassword && errors.oldPassword}
                helperText={touched.oldPassword && errors.oldPassword}
              />
            </Stack>
            <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
              <TextField
                fullWidth
                type="password"
                label="New password"
                {...getFieldProps('newPassword')}
                error={touched.newPassword && errors.newPassword}
                helperText={touched.newPassword && errors.newPassword}
              />
              <TextField
                fullWidth
                type="password"
                label="Confirm new password"
                {...getFieldProps('repeatNewPassword')}
                error={touched.repeatNewPassword && errors.repeatNewPassword}
                helperText={touched.repeatNewPassword && errors.repeatNewPassword}
              />
            </Stack>
          </Stack>
          <Box sx={{ mt: 3, display: 'flex', justifyContent: 'flex-end' }}>
            <LoadingButton type="submit" variant="contained" loading={isSubmitting} disabled={!isValid}>
              Save Changes
            </LoadingButton>
          </Box>
        </Card>
      </Form>
    </FormikProvider>
  );
}
