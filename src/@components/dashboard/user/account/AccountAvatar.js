import { Icon, Box, Typography, Paper, Card } from '@mui/material';
import { useDropzone } from 'react-dropzone';
// material
import { alpha, styled } from '@mui/material/styles';

import useAuth from 'hooks/useAuth';
import { LoadingButton } from '@mui/lab';
import { Form, FormikProvider, useFormik } from 'formik';
import { isString } from 'lodash';
import { FileService, UserService } from 'services';
import { useToast } from 'hooks';
// ----------------------------------------------------------------------

const RootStyle = styled('div')(({ theme }) => ({
  width: 144,
  height: 144,
  margin: 'auto',
  borderRadius: '50%',
  padding: theme.spacing(1),
  border: `1px dashed ${theme.palette.grey[500_32]}`,
}));

const DropZoneStyle = styled('div')({
  zIndex: 0,
  width: '100%',
  height: '100%',
  outline: 'none',
  display: 'flex',
  overflow: 'hidden',
  borderRadius: '50%',
  position: 'relative',
  alignItems: 'center',
  justifyContent: 'center',
  '& > *': { width: '100%', height: '100%' },
  '&:hover': {
    cursor: 'pointer',
    '& .placeholder': {
      zIndex: 9,
    },
  },
});

const PlaceholderStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  position: 'absolute',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  color: theme.palette.text.secondary,
  backgroundColor: theme.palette.background.neutral,
  transition: theme.transitions.create('opacity', {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.shorter,
  }),
  '&:hover': { opacity: 0.72 },
}));

// ----------------------------------------------------------------------

export default function AccountAvatar() {
  const { getRootProps, getInputProps, isDragActive, isDragReject, fileRejections } = useDropzone({
    multiple: false,
    accept: 'image/jpeg, image/png',
    maxSize: 3145728,
    onDrop: (acceptedFiles) => {
      const file = acceptedFiles[0];
      if (file) {
        setFieldValue('avatar', file);
      }
    },
  });
  const [{ user }, { updateUser }] = useAuth();
  const { showSuccess, showError } = useToast();
  const ShowRejectionItems = () => (
    <Paper
      variant="outlined"
      sx={{
        py: 1,
        px: 2,
        my: 2,
        borderColor: 'error.light',
        bgcolor: (theme) => alpha(theme.palette.error.main, 0.08),
      }}
    >
      {fileRejections.map(({ file, errors }) => {
        const { path, size } = file;
        return (
          <Box key={path} sx={{ my: 1 }}>
            <Typography variant="subtitle2" noWrap>
              {path} - {size}
            </Typography>
            {errors.map((e) => (
              <Typography key={e.code} variant="caption" component="p">
                - {e.message}
              </Typography>
            ))}
          </Box>
        );
      })}
    </Paper>
  );

  const initialValues = {
    avatar: user?.avatar,
  };

  const onSubmit = async (values, { setErrors, setSubmitting }) => {
    try {
      setSubmitting(true);
      // Upload avatar
      const url = await FileService.upload(`avatar/${user.id}`, values.avatar);
      await UserService.updateUserAvatar(url);
      /* Set new user information in localStorage */
      updateUser({ ...user, avatar: url });
      showSuccess('Update avatar successfully');
    } catch (e) {
      showError(e.message || 'Upload avatar error');
      setErrors(e);
      setSubmitting(false);
    } finally {
      setSubmitting(false);
    }
  };

  const formik = useFormik({ initialValues, onSubmit });
  const { values, touched, handleSubmit, setFieldValue, errors, isSubmitting } = formik;
  return (
    <Card sx={{ p: 3 }}>
      <FormikProvider value={formik}>
        <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <RootStyle>
            <DropZoneStyle
              error={Boolean(touched.avatar && errors.avatar)}
              {...getRootProps()}
              sx={{
                ...(isDragActive && { opacity: 0.72 }),
                ...(isDragReject && {
                  color: 'error.main',
                  borderColor: 'error.light',
                  bgcolor: 'error.lighter',
                }),
              }}
            >
              <input {...getInputProps()} />

              {values.avatar && (
                <Box
                  component="img"
                  alt="avatar"
                  src={isString(values.avatar) ? values.avatar : URL.createObjectURL(values.avatar)}
                  sx={{ zIndex: 8, objectFit: 'cover' }}
                />
              )}

              <PlaceholderStyle
                className="placeholder"
                sx={{
                  ...(values.avatar && {
                    opacity: 0,
                    color: 'common.white',
                    bgcolor: 'grey.900',
                    '&:hover': { opacity: 0.72 },
                  }),
                }}
              >
                <Box component={Icon} children="add_a_photo" sx={{ width: 24, height: 24, mb: 1 }} />
                <Typography variant="caption">{values.avatar ? 'Update photo' : 'Upload photo'}</Typography>
              </PlaceholderStyle>
            </DropZoneStyle>
          </RootStyle>
          <Typography
            variant="caption"
            sx={{
              mt: 2,
              mx: 'auto',
              display: 'block',
              textAlign: 'center',
              color: 'text.secondary',
            }}
          >
            Allowed *.jpeg, *.jpg, *.png, *.gif
          </Typography>

          {fileRejections.length > 0 && <ShowRejectionItems />}

          <Box sx={{ mt: 3, display: 'flex', justifyContent: 'center' }}>
            <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
              Save Changes
            </LoadingButton>
          </Box>
        </Form>
      </FormikProvider>
    </Card>
  );
}
