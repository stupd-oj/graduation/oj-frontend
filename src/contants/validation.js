import i18next from 'i18next';
import * as Yup from 'yup';

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

export const userValidation = {
  username: Yup.string()
    .trim()
    .min(3, i18next.t('Username must be more than 3 characters'))
    .max(50, i18next.t('Username must be less than 50 characters'))
    .required(i18next.t('Username is required')),
  passwordLogin: Yup.string().required(i18next.t('Password is required')),
  password: Yup.string()
    .min(6, i18next.t('Password must be 6 characters or more'))
    .required(i18next.t('Password is required')),
  fullName: Yup.string()
    .trim()
    .min(2, i18next.t('Name is too short'))
    .max(50, i18next.t('Name is too long'))
    .required(i18next.t('Full name is required')),
  email: Yup.string().email().required(i18next.t('Email is required')),
  confirmPassword: Yup.string()
    .required()
    .oneOf([Yup.ref('password'), null], i18next.t('Confirm password must match')),
  confirmNewPassword: Yup.string()
    .required()
    .oneOf([Yup.ref('newPassword'), null], i18next.t('Confirm password must match')),
  phoneNumber: Yup.string().matches(phoneRegExp, i18next.t('Phone number is not valid')),
  code: Yup.string()
    .required(i18next.t('Code is required'))
    .matches(/^\d+$/, i18next.t('Please enter a valid code'))
    .test('len', i18next.t('Code must be exactly 6 digits'), (val) => val?.length === 6),
};

export const contestValidation = {
  name: Yup.string().required(i18next.t('Contest name is required')).max(255),
  description: Yup.string().max(5000),
  rule: Yup.string().required(i18next.t('Rule must be selected')),
  startAt: Yup.date().required('Start date is required'),
  endAt: Yup.date().required('End date is required').min(Yup.ref('startAt'), 'End date cannot before start date'),
  password: Yup.string().max(255),
};
