import { Link } from '@components';
import { Icon, IconButton } from '@mui/material';
import { ProblemLevel } from 'contants/problem';
import ROUTES from 'routes/paths';
import { formatDate } from 'utils';
import { USER_ROLES } from 'utils/constants';

export const ContestColumnDef = [
  {
    field: 'id',
    headerName: 'ID',
    flex: 1,
  },
  {
    field: 'name',
    headerName: 'Name',
    renderCell: ({ row, value }) => <Link to={ROUTES.CONTEST_DETAIL.replace(':contestId', row.id)}>{value}</Link>,
    flex: 3,
  },
  {
    field: 'rule',
    headerName: 'Rule',
    flex: 1,
  },
  {
    type: 'boolean',
    field: 'isPrivate',
    headerName: 'Is Private?',
    sortable: false,
    flex: 1,
  },
  {
    field: 'startAt',
    headerName: 'Start Time',
    valueFormatter: ({ value }) => formatDate(value, 'DDMMYYYYhhmm'),
    flex: 2,
  },
  {
    field: 'endAt',
    headerName: 'End Time',
    valueFormatter: ({ value }) => formatDate(value, 'DDMMYYYYhhmm'),

    flex: 2,
  },
  {
    field: 'closeRankAt',
    headerName: 'Close rank',
    valueFormatter: ({ value }) => formatDate(value, 'DDMMYYYYhhmm'),

    flex: 2,
  },
  {
    field: 'createdAt',
    headerName: 'Created At',
    valueFormatter: ({ value }) => formatDate(value, 'DDMMYYYYhhmmss'),
    flex: 2,
  },
  {
    field: 'numberOfContestants',
    headerName: 'Contestants',
    flex: 2,
  },
  {
    field: 'createdBy',
    headerName: 'Created By',
    renderCell: ({ value }) =>
      value && <Link to={ROUTES.USER_PROFILE.replace(':username', value.username)}>{value.fullName}</Link>,
    flex: 2,
    sortable: false,
  },
  {
    field: '_action',
    headerName: 'Update',
    renderCell: ({ row }) => (
      <Link to={ROUTES.ADMIN_UPDATE_CONTEST.replace(':contestId', row.id)}>
        <Icon children="edit" />
      </Link>
    ),
    flex: 1,
    sortable: false,
  },
];

export const UserColumnDef = [
  {
    field: 'id',
    headerName: 'ID',
    flex: 1,
  },
  {
    field: 'username',
    headerName: 'Username',
    renderCell: ({ value }) => <Link to={ROUTES.USER_PROFILE.replace(':username', value)}>{value}</Link>,
    flex: 3,
  },
  {
    field: 'fullName',
    headerName: 'Full Name',
    flex: 3,
  },
  {
    field: 'email',
    headerName: 'Email',
    flex: 3,
  },
  {
    field: 'phoneNumber',
    headerName: 'Phone Number',
    flex: 2,
  },
  {
    field: 'createdAt',
    headerName: 'Register Date',
    valueFormatter: ({ value }) => formatDate(value),
    flex: 2,
  },
  {
    field: 'role',
    headerName: 'Role',
    valueFormatter: ({ value }) => USER_ROLES[value],
    flex: 1,
  },
  {
    type: 'boolean',
    field: 'isVerified',
    headerName: 'Verified',
    flex: 1,
    sortable: false,
  },
  {
    field: '_action',
    headerName: 'Update',
    renderCell: ({ row }) => (
      <Link to={ROUTES.ADMIN_UPDATE_USER.replace(':userId', row.id)}>
        <Icon children="edit" />
      </Link>
    ),
    flex: 1,
    sortable: false,
  },
];

export const ProblemColumnDef = [
  {
    field: 'id',
    headerName: 'ID',
    flex: 1,
  },
  {
    field: 'code',
    headerName: 'Code',
    flex: 1,
  },
  {
    field: 'name',
    headerName: 'Name',
    renderCell: ({ value, row }) => <Link to={ROUTES.PROBLEM_DETAIL.replace(':problemId', row.id)}>{value}</Link>,
    flex: 3,
  },
  {
    field: 'level',
    headerName: 'Level',
    renderCell: ({ value }) => ProblemLevel[value],
    flex: 1,
  },
  {
    field: 'multiplier',
    headerName: 'Multiplier',
    flex: 1,
  },
  {
    type: 'boolean',
    field: 'active',
    headerName: 'Active',
    flex: 1,
    sortable: false,
  },
  {
    field: '_action',
    headerName: 'Update',
    renderCell: ({ row }) => (
      <IconButton onClick={row._update}>
        <Icon children="edit" />
      </IconButton>
    ),
    flex: 1,
  },
  {
    field: '_test_dialog',
    headerName: 'Test',
    renderCell: ({ row }) => (
      <IconButton onClick={row._test}>
        <Icon children="bolt" />
      </IconButton>
    ),
    flex: 1,
  },
];

export const ContestantColumnDef = [
  {
    field: 'user',
    headerName: 'User',
    renderCell: ({ value }) => (
      <Link
        to={ROUTES.USER_PROFILE.replace(':username', value.username)}
      >{`[${value.username}] ${value.fullName}`}</Link>
    ),
    flex: 1,
    sortable: false,
  },
  {
    field: 'createdAt',
    headerName: 'Joined At',
    valueFormatter: ({ value }) => formatDate(value, 'DDMMYYYYhhmm'),
    flex: 1,
    sortable: false,
  },
];
