// material
import { styled } from '@mui/material/styles';
import { Box, Typography, Container, Button } from '@mui/material';
// @components
import { ComingSoonIllustration } from 'assets';
import Page from '@components/Page';
import { MotionContainer, varBounceIn } from '@components/animate';
import { motion } from 'framer-motion';
import { Link as RouterLink } from 'react-router-dom';

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  minHeight: '100%',
  display: 'flex',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

// ----------------------------------------------------------------------

export default function ComingSoon() {
  return (
    <RootStyle title="Coming Soon">
      <Container>
        <MotionContainer initial="initial" open>
          <Box sx={{ maxWidth: 480, margin: 'auto', textAlign: 'center' }}>
            <motion.div variants={varBounceIn}>
              <Typography variant="h3" paragraph>
                Coming Soon!
              </Typography>
            </motion.div>
            <Typography sx={{ color: 'text.secondary' }}>We are currently working hard on this page!</Typography>

            <motion.div variants={varBounceIn}>
              <ComingSoonIllustration sx={{ height: 260, my: { xs: 5, sm: 10 } }} />
            </motion.div>

            <Button to="/" size="large" variant="contained" component={RouterLink}>
              Go to Home
            </Button>
          </Box>
        </MotionContainer>
      </Container>
    </RootStyle>
  );
}
