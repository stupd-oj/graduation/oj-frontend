import Page from '@components/Page';
import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import ROUTES from 'routes/paths';
import { CodeService } from 'services';
import { ContestList } from './components/ContestList';

export default function Contest() {
  return (
    <Page title="Contests">
      <HeaderBreadcrumbs
        links={[
          { name: 'Dashboard', href: ROUTES.DASHBOARD_ROOT },
          { name: 'Contests', href: ROUTES.CONTEST_LIST },
        ]}
      />
      <ContestList getDataFunc={CodeService.getContestList} />
    </Page>
  );
}
