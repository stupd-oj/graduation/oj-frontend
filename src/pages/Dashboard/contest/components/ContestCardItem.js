import { Avatar, Box, Button, Card, Grid, Link, Stack, Typography, Icon, colors } from '@mui/material';
import PropTypes from 'prop-types';
import { calculateTimeLeft, formatDate } from 'utils';
import AccessAlarmIcon from '@mui/icons-material/AccessAlarm';
import BadgeItem from '@components/BadgeItem';
import { Link as RouterLink } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import { useEffect, useState } from 'react';
import { CONTEST_STATUS } from 'utils/constants';
import ROUTES from 'routes/paths';

ContestCardItem.propTypes = {
  contest: PropTypes.object,
};

const IconWrapperStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  color: theme.palette.primary.main,
}));

export default function ContestCardItem({ contest }) {
  const [contestStatus, setContestStatus] = useState(CONTEST_STATUS.ENDED);
  const [timeLeft, setTimeLeft] = useState('Ended');
  let timeLeftInterval;

  useEffect(() => {
    if (new Date() < new Date(contest.startAt)) {
      setContestStatus(CONTEST_STATUS.UPCOMING);
      timeLeftInterval = setInterval(() => {
        setTimeLeft(`Start in ${calculateTimeLeft(contest.startAt)}`);
      }, 1000);
    } else if (new Date() <= new Date(contest.endAt)) {
      setContestStatus(CONTEST_STATUS.ONGOING);
      timeLeftInterval = setInterval(() => {
        setTimeLeft(`Finish in ${calculateTimeLeft(contest.endAt)}`);
      }, 1000);
    } else {
      clearInterval(timeLeftInterval);
      setTimeLeft(`Ended`);
    }
  }, [contest]);

  return (
    <Card sx={{ p: 3, backgroundColor: '' }}>
      <Grid container spacing={2} alignItems="center">
        <Grid item md={8} xs={12}>
          <Stack
            direction={{ xs: 'column', md: 'row' }}
            justifyContent="flex-start"
            alignItems="center"
            spacing={3}
            sx={{ width: '100%' }}
          >
            <Grid>
              <Avatar
                variant="rounded"
                alt="Contest image"
                src={contest.image}
                sx={{
                  height: 160,
                  width: {
                    md: 160,
                    xs: '100%',
                  },
                }}
              />
            </Grid>
            <Grid>
              <Box>
                <Box>
                  <Link
                    component={RouterLink}
                    to={ROUTES.CONTEST_DETAIL.replace(':contestId', contest.id)}
                    variant="body1"
                    underline="none"
                    gutterBottom
                    sx={{
                      fontSize: '2rem',
                    }}
                  >
                    {contest.name}
                  </Link>
                </Box>
                <Typography variant="subtitle1" sx={{ display: 'inline' }}>
                  Schedule:
                  <Typography variant="body2" sx={{ display: 'inline' }}>
                    {` ${formatDate(contest.startAt, 'DDMMYYYYhhmm')} - ${formatDate(contest.endAt, 'DDMMYYYYhhmm')}`}
                  </Typography>
                </Typography>
                <Typography variant="subtitle1">
                  Rule:
                  <BadgeItem name={contest.rule} type="info" sx={{ m: 0, ml: 1 }} />
                </Typography>
              </Box>
              <Stack direction="row" alignItems="center">
                <AccessAlarmIcon
                  fontSize="small"
                  color={contestStatus === CONTEST_STATUS.ENDED ? 'error' : 'success'}
                />
                <Typography
                  variant="subtitle1"
                  marginLeft={1}
                  color={contestStatus === CONTEST_STATUS.ENDED ? 'error' : colors.green['600']}
                  sx={{ display: 'inline' }}
                >
                  {timeLeft}
                </Typography>
              </Stack>
            </Grid>
          </Stack>
        </Grid>
        <Grid item md={4} xs={12}>
          <Stack spacing={3} direction="row" justifyContent="center" alignItems="center">
            <Box>
              <Stack direction="column" alignItems="center">
                <IconWrapperStyle>
                  <Icon children="groups" fontSize="large" />
                </IconWrapperStyle>
                <Typography>{contest.numberOfContestants} contestants</Typography>
              </Stack>
            </Box>
            <Box>
              {contestStatus === CONTEST_STATUS.ENDED && (
                <Button component={RouterLink} to={`${contest.id}`} variant="contained" size="large" color="error">
                  {contestStatus}
                </Button>
              )}
              {contestStatus === CONTEST_STATUS.ONGOING && (
                <Button component={RouterLink} to={`${contest.id}`} variant="contained" size="large" color="success">
                  {contestStatus}
                </Button>
              )}
              {contestStatus === CONTEST_STATUS.UPCOMING && (
                <Button component={RouterLink} to={`${contest.id}`} variant="contained" size="large" color="warning">
                  {contestStatus}
                </Button>
              )}
            </Box>
          </Stack>
        </Grid>
      </Grid>
    </Card>
  );
}
