import { Box, Grid, Pagination, Typography } from '@mui/material';
import { usePagination, useToast } from 'hooks';
import { useEffect, useMemo } from 'react';
import ContestCardItem from 'pages/Dashboard/contest/components/ContestCardItem';

export function ContestList({ getDataFunc = () => {} }) {
  const [{ filter, data, total }, { getData, setFilterField }] = usePagination({
    getDataFunc,
    defaultPageSize: 5,
  });

  const { showError } = useToast();

  const totalPages = useMemo(() => Math.ceil(total / filter.pageSize), [total, filter]);

  useEffect(() => {
    getData(filter).catch((e) => {
      showError(e.message);
    });
  }, [filter]);

  return (
    <>
      <Grid container spacing={3} mt={2}>
        {data.map((e) => (
          <Grid item key={e.id} xs={12}>
            <ContestCardItem key={e.id} contest={e} />
          </Grid>
        ))}
        {data.length === 0 && (
          <Grid item xs={12}>
            <Typography variant="subtitle2" align="center">
              No contest found
            </Typography>
          </Grid>
        )}
      </Grid>

      {totalPages > 1 && (
        <Box mt={3} justifyContent="center" display="flex">
          <Pagination
            page={filter.page + 1}
            count={totalPages}
            onChange={(e, page) => setFilterField('page')(page - 1)}
          />
        </Box>
      )}
    </>
  );
}
