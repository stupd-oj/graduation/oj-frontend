import { alpha, styled } from '@mui/material/styles';
import { Button, Card, CardContent, Box, Typography, Icon, OutlinedInput } from '@mui/material';
import { useTheme } from '@mui/styles';
import { useAppState, useToast } from 'hooks';
import { useState } from 'react';
import { CodeService } from 'services';
import { formatDate } from 'utils';

const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: 'none',
  textAlign: 'center',
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up('md')]: {
    height: '100%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
}));

export default function ContestWelcome({ contest, postJoin = {} }) {
  const { joined, joinable, image, name, numberOfContestants, startAt, endAt, rule } = contest ?? {};
  const theme = useTheme();

  const [password, setPassword] = useState('');
  const [{ loading }, { hideLoading, showLoading }] = useAppState();
  const { showSuccess, showError } = useToast();

  const joinContest = async () => {
    showLoading();
    try {
      await CodeService.joinContest(contest.id, { password });
      showSuccess('Join contest successfully');
      await postJoin();
    } catch (e) {
      showError(e.message);
    }
    hideLoading();
  };

  return (
    <RootStyle>
      <CardContent sx={{ color: 'grey.800', height: 300, width: '70%', display: 'flex', alignItems: 'center' }}>
        <Box>
          <Typography gutterBottom variant="h3" color="primary.darker">
            {name}
          </Typography>
          <Box display="flex" alignItems="center" mb={1} color="primary.darker">
            <Icon children="groups" />
            <Typography variant="subtitle2" ml={1} color="primary.dark">
              {numberOfContestants}
            </Typography>
          </Box>
          <Box display="flex" alignItems="center" mb={1} color="primary.darker">
            <Icon children="schedule" />
            <Typography variant="subtitle2" ml={1} color="primary.dark">
              {`${formatDate(startAt, 'DDMMYYYYhhmm')} - ${formatDate(endAt, 'DDMMYYYYhhmm')}`}
            </Typography>
          </Box>
          <Box display="flex" alignItems="center" mb={2} color="primary.darker">
            <Icon children="gavel" />
            <Typography variant="subtitle2" ml={1} color="primary.dark">
              {rule}
            </Typography>
          </Box>

          <Box display="flex" alignItems="center">
            {!joined && (
              <>
                {!joinable && (
                  <OutlinedInput
                    size="small"
                    value={password}
                    placeholder="Contest password"
                    onChange={(e) => setPassword(e?.target?.value)}
                    sx={{
                      mr: 2,
                      color: 'common.white',
                      fontWeight: 'fontWeightMedium',
                      bgcolor: (theme) => alpha(theme.palette.common.black, 0.16),
                      '& input::placeholder': {
                        color: (theme) => alpha(theme.palette.common.white, 0.8),
                      },
                      '& fieldset': { display: 'none' },
                    }}
                  />
                )}
                <Button variant="contained" onClick={joinContest} disabled={loading} color="primary">
                  Join Now
                </Button>
              </>
            )}
          </Box>
        </Box>
      </CardContent>
      <Box
        position="absolute"
        height="100%"
        width="100%"
        sx={{
          zIndex: -1,
          backgroundImage: `linear-gradient(to right, ${theme?.palette?.primary?.light} 20%, transparent 100%), url(${image})`,
          backgroundSize: 'contain',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'right center',
        }}
      />
    </RootStyle>
  );
}
