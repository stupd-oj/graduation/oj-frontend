import Page from '@components/Page';
import { Skeleton } from '@components/Skeleton';
import { Outlet, useLocation, useNavigate, useParams } from 'react-router-dom';
import { useEffect, useMemo, useState } from 'react';
import { CodeService } from 'services';
import { Box, Icon, Tab, Tabs } from '@mui/material';
import ROUTES from 'routes/paths';
import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import ContestWelcome from './components/ContestWelcome';

const CONTEST_TABS = [
  {
    key: 'default',
    value: 'Information',
    icon: <Icon children="info" />,
    route: ROUTES.CONTEST_DETAIL,
  },
  {
    key: 'leaderboard',
    value: 'Leaderboard',
    icon: <Icon children="leaderboard" />,
    route: ROUTES.CONTEST_LEADERBOARD,
  },
  {
    key: 'problems',
    value: 'Problems',
    icon: <Icon children="code" />,
    route: ROUTES.CONTEST_PROBLEM_LIST,
  },
  {
    key: 'submissions',
    value: 'Submission',
    icon: <Icon children="history_edu" />,
    route: ROUTES.CONTEST_SUBMISSION_LIST,
  },
];

export default function ContestDetail() {
  const { contestId } = useParams();
  const [contest, setContest] = useState(null);
  const location = useLocation();
  const navigate = useNavigate();
  const currentTab = useMemo(() => {
    const { pathname } = location;
    return CONTEST_TABS?.find(({ key }) => pathname.includes(key)) ?? CONTEST_TABS[0];
  }, [location]);

  const getData = async () => {
    try {
      const { data } = await CodeService.getContestDetail(contestId);
      setContest(data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(getData, [contestId]);

  return (
    <Page title={`${currentTab.value} | ${contest?.name ?? 'Contest'}`}>
      <HeaderBreadcrumbs
        links={[
          { name: 'Dashboard', href: ROUTES.DASHBOARD_ROOT },
          { name: 'Contests', href: ROUTES.CONTEST_LIST },
          { name: contest?.name },
        ]}
      />
      <Skeleton loading={!contest}>
        <ContestWelcome contest={contest} postJoin={getData} />
      </Skeleton>
      {contest && (
        <>
          <Box sx={{ width: '100%', borderBottom: 1, borderColor: 'divider', my: 3 }}>
            <Tabs
              variant="fullWidth"
              value={currentTab.key}
              onChange={(e, value) => {
                const route = CONTEST_TABS?.find(({ key }) => key === value)?.route ?? CONTEST_TABS[0].route;
                navigate(route.replace(':contestId', contestId));
              }}
            >
              {CONTEST_TABS.map(({ value, icon, key }) => (
                <Tab disableRipple key={value} label={value} icon={icon} value={key} />
              ))}
            </Tabs>
          </Box>
          <Outlet context={{ contest }} />
        </>
      )}
    </Page>
  );
}
