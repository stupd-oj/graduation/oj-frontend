import { Box, Button, Card, FormControl, IconButton, InputLabel, Select, Stack, Tooltip } from '@mui/material';
import { useRef, useState } from 'react';
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/pascal/pascal';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/scroll/simplescrollbars.css';
import { CODE_TEMPLATE, PROGRAM_LANGUAGE } from 'utils/constants';
import UploadIcon from '@mui/icons-material/Upload';
import RefreshIcon from '@mui/icons-material/Refresh';
import { useParams, useNavigate } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import { useAppState, useToast } from 'hooks';
import { CodeService } from 'services';
import ROUTES from 'routes/paths';

export default function SubmissionCreateForm({ contestId }) {
  const [options, setOptions] = useState({
    mode: PROGRAM_LANGUAGE.C.type,
    theme: 'material',
    lineNumbers: true,
    simplescrollbars: true,
    scrollbarStyle: 'simple',
  });
  const [source, setSource] = useState(CODE_TEMPLATE.C);
  const { problemId } = useParams();
  const [language, setLanguage] = useState(PROGRAM_LANGUAGE.C.name);
  const [{ loading }, { showLoading, hideLoading }] = useAppState();
  const { showSuccess, showError } = useToast();
  const navigate = useNavigate();
  const fileInputRef = useRef(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    showLoading();
    try {
      const payload = {
        problemId: Number(problemId),
        source,
        language,
      };
      const { data } = await CodeService.createSubmission(payload);
      showSuccess(`Submit success: #${data}`);
      navigate(ROUTES.CONTEST_SUBMISSION_LIST.replace(':contestId', contestId), { replace: true });
    } catch (e) {
      showError(e.message);
    }
    hideLoading();
  };

  const handleResetToDefault = () => {
    setSource(CODE_TEMPLATE[language]);
  };

  const handleAttach = () => {
    fileInputRef.current.click();
  };

  const handleLanguageChange = (e) => {
    setLanguage(PROGRAM_LANGUAGE[e.target.value].name);
    setSource(CODE_TEMPLATE[e.target.value]);
    const newOptions = { ...options };
    newOptions.mode = PROGRAM_LANGUAGE[e.target.value].type;
    setOptions(newOptions);
  };

  const handleFileChange = (e) => {
    const file = e?.target?.files[0];
    const fileReader = new FileReader();
    fileReader.readAsText(file);
    fileReader.onloadend = () => {
      setSource(fileReader.result.toString());
    };
  };

  return (
    <Card sx={{ p: 3 }}>
      <form onSubmit={handleSubmit}>
        <Stack direction="row" spacing={3} justifyContent="space-between">
          <Stack direction="row" spacing={1}>
            <FormControl size="small">
              <InputLabel>Language</InputLabel>
              <Select label="Language" native onChange={handleLanguageChange}>
                {Object.values(PROGRAM_LANGUAGE).map((language) => (
                  <option key={language.name} value={language.name}>
                    {language.name}
                  </option>
                ))}
              </Select>
            </FormControl>
            <Tooltip title="Reset to default code" placement="top" arrow>
              <IconButton onClick={handleResetToDefault}>
                <RefreshIcon />
              </IconButton>
            </Tooltip>
          </Stack>
          <Stack>
            <Button variant="outlined" startIcon={<UploadIcon />} onClick={handleAttach}>
              Upload
            </Button>
            <input ref={fileInputRef} type="file" style={{ display: 'none' }} onChange={handleFileChange} />
          </Stack>
        </Stack>
        <Box sx={{ my: 3 }}>
          <CodeMirror
            value={source}
            options={options}
            onBeforeChange={(editor, data, value) => {
              setSource(value);
            }}
          />
        </Box>
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'flex-end' }}>
          <LoadingButton loading={loading} variant="contained" type="submit" size="large">
            Submit
          </LoadingButton>
        </Box>
      </form>
    </Card>
  );
}
