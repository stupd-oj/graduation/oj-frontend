import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import Page from '@components/Page';
import { Skeleton } from '@components/Skeleton';
import { ProblemLevel } from 'contants/problem';
import { useParams, Link as RouterLink } from 'react-router-dom';
import { Card, CardContent, CardHeader, Grid, Link, Stack, Typography } from '@mui/material';
import ROUTES from 'routes/paths';
import { CodeService } from 'services';
import { useEffect, useState } from 'react';
import Markdown from '@components/Markdown';
import { styled } from '@mui/material/styles';
import { useToast } from 'hooks/useToast';
import SubmissionCreateForm from './components/SubmissionCreateForm';

const RowStyle = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
});

function ProblemDetailCard({ name, description }) {
  return (
    <Card>
      <CardHeader title={name} />
      <CardContent sx={{ color: 'text.primary' }}>
        <Markdown children={description} />
      </CardContent>
    </Card>
  );
}

function ProblemInfoCard({ code, memoryLimit, timeLimit, contest, level, createdBy }) {
  return (
    <Card>
      <CardContent>
        <Stack spacing={2}>
          <RowStyle>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              Contest
            </Typography>
            <Link component={RouterLink} to={ROUTES.CONTEST_DETAIL.replace(':contestId', contest?.id)}>
              <Typography variant="body2">{contest?.name}</Typography>
            </Link>
          </RowStyle>
          <RowStyle>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              Code
            </Typography>
            <Typography variant="body2">{code}</Typography>
          </RowStyle>
          <RowStyle>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              Time limit
            </Typography>
            <Typography variant="body2">{`${timeLimit}s`}</Typography>
          </RowStyle>
          <RowStyle>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              Memory limit
            </Typography>
            <Typography variant="body2">{`${memoryLimit} MB`}</Typography>
          </RowStyle>
          <RowStyle>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              Level
            </Typography>
            <Typography variant="body2">{ProblemLevel[level]}</Typography>
          </RowStyle>
          <RowStyle>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              Created by
            </Typography>
            <Link component={RouterLink} to={ROUTES.USER_PROFILE.replace(':username', createdBy?.username)}>
              <Typography variant="body2">{createdBy?.fullName}</Typography>
            </Link>
          </RowStyle>
        </Stack>
      </CardContent>
    </Card>
  );
}

export default function ProblemDetail() {
  const { problemId } = useParams();
  const { showError } = useToast();

  const [problem, setProblem] = useState();

  const getData = async () => {
    try {
      const { data } = await CodeService.getProblemDetail(problemId);
      setProblem(data);
    } catch (e) {
      showError(e.message);
    }
  };

  useEffect(getData, [problemId]);
  return (
    <Page title={`${problem?.name ?? 'Loading...'} | IT PTIT Code`}>
      <HeaderBreadcrumbs
        links={[
          { name: 'Dashboard', href: ROUTES.DASHBOARD_ROOT },
          {
            name: problem?.contest?.name,
            href: ROUTES.CONTEST_DETAIL.replace(':contestId', problem?.contestId),
          },
          { name: problem?.name },
        ]}
      />
      <Grid container spacing={3}>
        <Grid item xs={12} md={8}>
          <Stack spacing={3}>
            <Skeleton loading={!problem} height={300}>
              <ProblemDetailCard {...problem} />
            </Skeleton>
            <Skeleton loading={!problem} height={300}>
              {problem?.canSubmit && <SubmissionCreateForm contestId={problem?.contestId} />}
            </Skeleton>
          </Stack>
        </Grid>
        <Grid item xs={12} md={4}>
          <Skeleton loading={!problem} height={300}>
            <ProblemInfoCard {...problem} />
          </Skeleton>
        </Grid>
      </Grid>
    </Page>
  );
}
