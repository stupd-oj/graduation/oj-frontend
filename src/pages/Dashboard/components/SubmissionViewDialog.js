import { Page } from '@components';
import { Dialog, DialogContent, DialogTitle } from '@mui/material';
import { useAppState, useToast } from 'hooks';
import useAuth from 'hooks/useAuth';
import { forwardRef, useImperativeHandle, useMemo, useState } from 'react';
import { Controlled as CodeMirror } from 'react-codemirror2';
import { CodeService } from 'services';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/python/python';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/scroll/simplescrollbars.css';
import { PROGRAM_LANGUAGE } from 'utils/constants';

export const SubmissionViewDialog = forwardRef((props, ref) => {
  const [isOpen, setOpen] = useState(false);
  const { showError } = useToast();
  const [submission, setSubmission] = useState(null);
  const [, { hideLoading, showLoading }] = useAppState();
  const [{ user }] = useAuth();

  const view = async (submissionId) => {
    setSubmission(false);
    setOpen(true);
    showLoading();

    try {
      let data;
      if (user.role > 0) {
        const response = await CodeService.getAdminSubmissionDetail(submissionId);
        data = response.data;
      } else {
        const response = await CodeService.getSubmissionDetail(submissionId);
        data = response.data;
      }
      setSubmission(data);
    } catch (e) {
      showError(e.message);
      setOpen(false);
    }
    hideLoading();
  };

  useImperativeHandle(ref, () => ({
    view,
  }));

  const editorOptions = useMemo(
    () => ({
      mode: PROGRAM_LANGUAGE?.[submission?.language]?.type,
      theme: 'material',
      lineNumbers: true,
      simplescrollbars: true,
      scrollbarStyle: 'simple',
    }),
    [submission],
  );

  return (
    <Dialog open={isOpen} maxWidth="md" fullWidth onClose={() => setOpen(false)}>
      <Page container={false}>
        <DialogTitle>#{submission?.id}</DialogTitle>
        <DialogContent>
          <CodeMirror options={editorOptions} value={submission?.source} onBeforeChange={() => {}} />
        </DialogContent>
      </Page>
    </Dialog>
  );
});
