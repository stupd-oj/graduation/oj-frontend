import { useEffect, useState } from 'react';
import Page from '@components/Page';
import { Box, Typography } from '@mui/material';
import { CodeService } from 'services';
import { ContestList } from './contest/components/ContestList';
import ContestCardItem from './contest/components/ContestCardItem';

export default function GeneralApp() {
  const [practiceContest, setPracticeContest] = useState(null);
  useEffect(() => {
    CodeService.getContestDetail(0)
      .then(({ data }) => setPracticeContest(data))
      .catch();
  }, []);

  return (
    <Page title="General">
      {practiceContest && <ContestCardItem contest={practiceContest} />}
      <Box my={3}>
        <Typography variant="h6">Joined contests</Typography>
        <ContestList getDataFunc={CodeService.getContestJoined} />
      </Box>
    </Page>
  );
}
