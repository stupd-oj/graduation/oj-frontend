import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import { userValidation } from 'contants/validation';
import { useEffect, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import ROUTES from 'routes/paths';
import { Button, FormControlLabel, FormLabel, Grid, Paper, Radio, RadioGroup } from '@mui/material';
import Page from '@components/Page';
import { useToast } from 'hooks/useToast';
import { UserService } from 'services';
import { Formik, useFormikContext, Form as FormikForm } from 'formik';
import FormikInput from '@components/FormikInput';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { FormikDatePicker } from '@components';

const UpdateUser = () => {
  const { userId } = useParams();
  const [user, setUser] = useState();
  const [loading, setLoading] = useState(false);
  const { showError, showSuccess } = useToast();

  const getData = async (_id) => {
    setLoading(true);
    try {
      const { data } = await UserService.getUserById(_id);
      setUser(data);
    } catch (e) {
      showError(e.message);
    }
    setLoading(false);
  };

  useEffect(() => {
    getData(userId);
  }, [userId]);

  const onSubmit = async (payload) => {
    setLoading(true);
    try {
      await UserService.updateUser(userId, payload);
      showSuccess('Update user successfully');
      getData(userId);
    } catch (e) {
      showError(e.message);
    }
    setLoading(false);
  };

  const initialValues = useMemo(
    () => ({
      username: user?.username ?? '',
      fullName: user?.fullName ?? '',
      birthday: user?.birthday ? new Date(user?.birthday) : null,
      schoolName: user?.schoolName ?? '',
      className: user?.className ?? '',
      studentId: user?.studentId ?? '',
      mood: user?.mood ?? '',
      email: user?.email ?? '',
      phoneNumber: user?.phoneNumber ?? '',
      gender: user?.gender,
      role: user?.role ?? 0,
    }),
    [user],
  );

  const validationSchema = Yup.object().shape({
    email: userValidation.email,
    username: userValidation.username,
    fullName: userValidation.fullName,
    phoneNumber: userValidation.phoneNumber,
  });

  return (
    <Page title="User management" loading={loading}>
      <HeaderBreadcrumbs
        heading="User management"
        links={[
          { name: 'Admin', href: ROUTES.ADMIN_ROOT },
          { name: 'User Management', href: ROUTES.ADMIN_USER_LIST },
          { name: user?.username },
        ]}
      />
      <Paper variant="outlined" sx={{ p: 2 }}>
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
          enableReinitialize
        >
          <Form />
        </Formik>
      </Paper>
    </Page>
  );
};

export default UpdateUser;

const Form = () => {
  const { values, setFieldValue } = useFormikContext();
  const { t } = useTranslation();
  const navigate = useNavigate();

  return (
    <FormikForm>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <FormikInput type="text" name="username" fullWidth label={t('Username')} />
        </Grid>
        <Grid item xs={6}>
          <FormikInput type="email" name="email" fullWidth label={t('Email')} />
        </Grid>
        <Grid item xs={6}>
          <FormikInput type="text" name="fullName" fullWidth label={t('Full name')} />
        </Grid>
        <Grid item xs={6}>
          <FormikInput type="tel" name="phoneNumber" fullWidth label={t('Phone number')} />
        </Grid>
        <Grid item xs={6}>
          <FormikDatePicker name="birthday" label="Birthday" inputFormat="dd/MM/yyyy" />
        </Grid>
        <Grid item xs={6}>
          <FormLabel component="legend">Gender</FormLabel>
          <RadioGroup
            row
            aria-label="Gender"
            name="gender"
            onChange={(e) => {
              setFieldValue('gender', e?.target?.value === 'true');
            }}
          >
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="Male"
              checked={values.gender !== null && !values.gender}
            />
            <FormControlLabel
              value="true"
              control={<Radio />}
              label="Female"
              checked={values.gender !== null && values.gender}
            />
          </RadioGroup>
        </Grid>
        <Grid item xs={12}>
          <FormikInput type="text" name="schoolName" fullWidth label={t('School')} />
        </Grid>
        <Grid item xs={6}>
          <FormikInput type="text" name="className" fullWidth label={t('Class')} />
        </Grid>
        <Grid item xs={6}>
          <FormikInput type="text" name="studentId" fullWidth label={t('Student ID')} />
        </Grid>
        <Grid item xs={6}>
          <FormLabel component="legend">{t('Role')}</FormLabel>
          <RadioGroup
            row
            aria-label="Role"
            name="role"
            onChange={(e) => {
              setFieldValue('role', +e?.target?.value);
            }}
          >
            <FormControlLabel value="0" control={<Radio />} label="User" checked={values.role === 0} />
            <FormControlLabel value="1" control={<Radio />} label="Admin" checked={values.role === 1} />
          </RadioGroup>
        </Grid>
        <Grid item xs={12} display="flex" justifyContent="center">
          <Button variant="contained" color="error" sx={{ mr: 2 }} type="button" onClick={() => navigate(-1)}>
            Cancel
          </Button>
          <Button variant="contained" type="submit">
            Save
          </Button>
        </Grid>
      </Grid>
    </FormikForm>
  );
};
