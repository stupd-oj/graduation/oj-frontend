import { Icon, IconButton, Menu, MenuItem } from '@mui/material';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ROUTES from 'routes/paths';

export const UserAction = ({ row }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const navigate = useNavigate();

  return (
    <div>
      <IconButton onClick={handleOpen}>
        <Icon children="more_vert" />
      </IconButton>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={() => navigate(ROUTES.ADMIN_UPDATE_USER.replace(':id', row.id))}>Edit</MenuItem>
        <MenuItem onClick={() => {}} disabled>
          Ban
        </MenuItem>
      </Menu>
    </div>
  );
};
