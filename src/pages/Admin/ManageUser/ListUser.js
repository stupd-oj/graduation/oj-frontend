import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import Page from '@components/Page';
import { Box, Icon, IconButton, InputAdornment, Paper, TextField, Tooltip } from '@mui/material';
import { DataGridPremium } from '@mui/x-data-grid-premium';
import { useCallback, useEffect } from 'react';
import { UserColumnDef } from 'contants/columnDefinition';
import { usePagination, useToast } from 'hooks';
import { debounce } from 'lodash';
import ROUTES from 'routes/paths';
import { UserService } from 'services';

export const ListUser = () => {
  const [{ filter, data, total }, { setFilterField, getData, setFilter }] = usePagination({
    getDataFunc: UserService.getListUsers,
    sortFieldMap: (fieldName) => `u.${fieldName}`,
  });
  const { showError } = useToast();

  const getDataDebounced = useCallback(
    debounce(async (_filter) => {
      try {
        await getData(_filter);
      } catch (e) {
        showError(e.message);
      }
    }, 500),
    [],
  );

  useEffect(() => {
    getDataDebounced(filter);
  }, [filter]);

  return (
    <Page title="User management">
      <HeaderBreadcrumbs
        heading="User management"
        links={[
          { name: 'Admin', href: ROUTES.ADMIN_ROOT },
          { name: 'User Management', href: ROUTES.ADMIN_USER_LIST },
        ]}
      />
      <Paper variant="outlined" sx={{ px: 2, mt: 3 }}>
        <Toolbar query={filter.query} setQuery={setFilterField('query')} getData={() => getDataDebounced(filter)} />
        <DataGridPremium
          autoHeight
          disableSelectionOnClick
          sortingMode="server"
          paginationMode="server"
          density="comfortable"
          pagination
          disableColumnMenu
          columns={UserColumnDef}
          rows={data}
          pageSize={filter.pageSize}
          sortModel={filter.sortModel}
          rowCount={total}
          onPaginationModelChange={(paginationModel) => setFilter({ ...filter, ...paginationModel })}
          onSortModelChange={setFilterField('sortModel')}
          pageSizeOptions={[10, 20, 50]}
        />
      </Paper>
    </Page>
  );
};

export default ListUser;

const Toolbar = ({ query, setQuery, getData }) => (
  <Box sx={{ mt: 2 }} display="flex" alignItems="end" justifyContent="end">
    <Tooltip title="Refresh">
      <IconButton onClick={getData} sx={{ mr: 2 }}>
        <Icon children="autorenew" />
      </IconButton>
    </Tooltip>
    <TextField
      type="search"
      value={query}
      onChange={(e) => setQuery(e.target.value)}
      size="small"
      placeholder="Search by name, email,..."
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Icon children="search" />
          </InputAdornment>
        ),
      }}
    />
  </Box>
);
