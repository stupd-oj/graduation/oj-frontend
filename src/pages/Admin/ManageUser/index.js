import Loadable from '@components/Loadable';
import { lazy } from 'react';

export const ListUser = Loadable(lazy(() => import('./ListUser')));
export const UpdateUser = Loadable(lazy(() => import('./UpdateUser')));
