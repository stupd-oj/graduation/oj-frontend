import { Page } from '@components';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { useAppState, useToast } from 'hooks';
import { forwardRef, useCallback, useEffect, useImperativeHandle, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DataGridPremium, useGridApiRef } from '@mui/x-data-grid-premium';
import { CodeService } from 'services';

const columns = [
  {
    field: 'id',
    headerName: 'ID',
    flex: 1,
  },
  {
    field: 'contest',
    headerName: 'Contest',
    valueGetter: ({ value }) => value?.name,
    flex: 3,
  },
  {
    field: 'code',
    headerName: 'Code',
    flex: 1,
  },
  {
    field: 'name',
    headerName: 'Name',
    flex: 3,
  },
  {
    field: 'level',
    headerName: 'Level',
    flex: 1,
  },
  {
    field: 'multiplier',
    headerName: 'Multiplier',
  },
];

export const ProblemImportDialog = forwardRef(({ onSubmit }, ref) => {
  const [, { showLoading, hideLoading }] = useAppState();
  const [isOpen, setOpen] = useState(false);
  const { showError } = useToast();
  const { t } = useTranslation();
  const [data, setData] = useState([]);
  const gridApiRef = useGridApiRef();
  const [selected, setSelected] = useState(null);

  const open = () => setOpen(true);
  const close = () => setOpen(false);

  useImperativeHandle(ref, () => ({
    open,
  }));

  useEffect(async () => {
    if (!isOpen) return;
    showLoading();
    try {
      const response = await CodeService.getAllProblem();
      setData(response.data);
      setSelected(null);
    } catch (e) {
      showError(e);
    }
    hideLoading();
  }, [isOpen]);

  const _onSubmit = useCallback(async () => {
    showLoading();
    try {
      const response = await CodeService.adminGetProblemDetail(selected);
      onSubmit(response.data);
      close();
    } catch (e) {
      showError(e);
    }
    hideLoading();
  }, [selected]);

  return (
    <Dialog open={isOpen} onClose={close} fullWidth maxWidth="md">
      <Page maxHeight="100%" container={false}>
        <DialogTitle>{t`Import problem`}</DialogTitle>
        <DialogContent>
          <Box height="80vh">
            <DataGridPremium
              columns={columns}
              rows={data}
              disableMultipleRowSelection
              pagination
              paginationMode="client"
              apiRef={gridApiRef}
              onRowSelectionModelChange={(e) => setSelected(e[0])}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button type="button" onClick={close} color="error">
            {t('Cancel')}
          </Button>
          <Button type="button" autoFocus variant="contained" disabled={!selected} onClick={_onSubmit}>
            {t('Import #{{id}}', { id: selected })}
          </Button>
        </DialogActions>
      </Page>
    </Dialog>
  );
});
