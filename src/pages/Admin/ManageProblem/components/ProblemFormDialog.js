import { FormikInput, Page } from '@components';
import Markdown from '@components/Markdown';
import { LoadingButton } from '@mui/lab';
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Grid,
  Icon,
  IconButton,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
} from '@mui/material';
import axios from 'axios';
import { MAX_TESTS, ProblemLevel } from 'contants/problem';
import { FieldArray, Form as FormikForm, Formik, useFormikContext } from 'formik';
import { useAppState, useConfirmationDialog, useToast } from 'hooks';
import { isNil } from 'lodash';
import { forwardRef, useImperativeHandle, useRef, useState } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { useTranslation } from 'react-i18next';
import { CodeService, FileService } from 'services';
import truncate from 'utils/truncate';
import makeRandom from 'utils/makeRandom';
import { ProblemImportDialog } from './ProblemImport';

export const ProblemFormDialog = forwardRef(({ contest, postSubmit }, ref) => {
  const [, { showLoading, hideLoading }] = useAppState();
  const [problemId, setProblemId] = useState();
  const [problem, setProblem] = useState();
  const [isOpen, setOpen] = useState(false);
  const { showError, showSuccess } = useToast();
  const importDialogRef = useRef({});

  const initForm = async (_problemId) => {
    showLoading();
    try {
      if (isNil(_problemId)) {
        setProblem(null);
      } else {
        const { data } = await CodeService.adminGetProblemDetail(_problemId);
        setProblem(data);
      }
    } catch (e) {
      showError(e.message);
    }
    hideLoading();
  };

  const open = (problemId) => {
    setProblemId(problemId);
    setOpen(true);
    initForm(problemId).catch();
  };

  const close = () => setOpen(false);

  const onSubmit = async (value) => {
    showLoading();
    try {
      const isAllIOUploaded = value?.tests?.every(
        (test) => (test.input || test.inputUri) && (test.output || test.outputUri),
      );
      if (!isAllIOUploaded) {
        throw new Error('You need to upload input/output files');
      }
      // Serialize before sending
      value.tests = await Promise.all(
        value.tests.map(async (test) => {
          if (test.input) {
            test.inputUri = await FileService.upload(test.inputUri ?? `testcases/${makeRandom(64)}`, test.input);
            delete test.input;
          }
          if (test.output) {
            test.outputUri = await FileService.upload(test.outputUri ?? `testcases/${makeRandom(64)}`, test.output);
            delete test.output;
          }
          return test;
        }),
      );
      setProblem((p) => ({ ...p, tests: value.tests }));
      value.code = value?.code?.toUpperCase();

      // Handling call API
      if (isNil(problemId)) {
        await CodeService.createProblem(value);
        showSuccess('Problem created successfully');
        close();
      } else {
        await CodeService.updateProblem(problemId, value);
        showSuccess('Problem updated successfully');
      }

      postSubmit();
    } catch (e) {
      showError(e.message);
    }
    hideLoading();
  };

  const onRemove = async () => {
    showLoading();
    try {
      if (problemId) {
        await CodeService.removeProblem(problemId);
        showSuccess('Problem removed successfully');
        close();
        postSubmit();
      }
    } catch (e) {
      showError(e.message);
    }
    hideLoading();
  };

  useImperativeHandle(ref, () => ({
    open,
  }));

  const onImportProblem = (_problem) => {
    console.log(_problem);
    setProblem({
      ..._problem,
      id: null,
      startAt: null,
      endAt: null,
    });
  };

  const initialValues = {
    id: problem?.id,
    name: problem?.name ?? '',
    description: problem?.description ?? '',
    code: problem?.code ?? '',
    hint: problem?.hint ?? '',
    level: problem?.level ?? ProblemLevel.EASY,
    multiplier: problem?.multiplier ?? 1,
    timeLimit: problem?.timeLimit ?? 2,
    memoryLimit: problem?.memoryLimit ?? 256,
    active: problem?.active ?? true,
    tests: problem?.tests ?? [],
    contestId: contest?.id,
    startAt: contest ? new Date(problem?.startAt ?? contest.startAt) : null,
    endAt: contest ? new Date(problem?.endAt ?? contest.endAt) : null,
    contestEndAt: contest ? new Date(contest.endAt) : null,
    contestStartAt: contest ? new Date(contest.startAt) : null,
  };

  return (
    <Dialog open={isOpen} onClose={close} fullWidth maxWidth="lg">
      <Page maxHeight="100%" display="flex" overflow="hidden" container={false}>
        <Formik initialValues={initialValues} onSubmit={onSubmit} enableReinitialize>
          <Form
            isCreate={isNil(problemId)}
            close={close}
            onRemove={onRemove}
            onImportProblemClick={() => importDialogRef.current.open()}
          />
        </Formik>
      </Page>
      <ProblemImportDialog onSubmit={onImportProblem} ref={importDialogRef} />
    </Dialog>
  );
});

const Form = ({ isCreate, close, onRemove, onImportProblemClick }) => {
  const [{ loading }, { showLoading, hideLoading }] = useAppState();
  const { t } = useTranslation();
  const { showError } = useToast();
  const { values, setFieldValue } = useFormikContext();
  const fileInputRef = useRef();
  const [fileField, setFileField] = useState(null);
  const [viewFileOpen, setViewFileOpen] = useState('');
  const [viewFileContent, setViewFileContent] = useState('');

  const onImportFiles = () => {
    setFileField();
    fileInputRef.current.click();
  };

  const onViewFile = async (file) => {
    setViewFileOpen(true);
    setViewFileContent('');

    if (typeof file === 'string') {
      showLoading();
      try {
        const fileContent = await axios.get(file).then((res) => res.data);
        setViewFileContent(fileContent);
      } catch (e) {
        showError(e);
        setViewFileOpen(false);
      }
      hideLoading();
    } else {
      const fileReader = new FileReader();
      fileReader.onload = () => setViewFileContent(fileReader.result);
      fileReader.readAsText(file);
    }
  };

  const onUploadFile = (fieldName) => () => {
    setFileField(fieldName);
    fileInputRef.current.click();
  };

  const onFileChange = (e) => {
    if (fileField) {
      setFieldValue(fileField, e?.target?.files?.[0]);
    } else {
      const files = Array.from(e?.target?.files);
      files.forEach((file) => {
        const { name: fileName } = file;
        // eslint-disable-next-line no-nested-ternary
        const typeOfFile = fileName.endsWith('.in') ? 'input' : fileName.endsWith('.out') ? 'output' : null;

        if (typeOfFile) {
          const testCaseNumber = fileName.split('.')[0];
          if (values?.tests?.[testCaseNumber]) {
            setFieldValue(`tests[${testCaseNumber}].${typeOfFile}`, file);
          }
        }
      });
    }
    e.target.value = null;
  };

  const RemoveDialog = useConfirmationDialog(onRemove);

  return (
    <FormikForm style={{ display: 'flex', flexDirection: 'column' }}>
      <DialogTitle>
        {isCreate ? (
          <>
            {t('Create problem')} <Button onClick={onImportProblemClick}>{t('Import from existed problem')}</Button>
          </>
        ) : (
          t('Update problem')
        )}
      </DialogTitle>
      <Dialog open={viewFileOpen} fullWidth maxWidth="md">
        <Page container={false}>
          <DialogContent>
            <TextField value={viewFileContent} readOnly multiline rows={5} fullWidth />
          </DialogContent>
          <DialogActions>
            <Button type="button" onClick={() => setViewFileOpen(false)} color="error">
              {t('Close')}
            </Button>
            <CopyToClipboard text={viewFileContent}>
              <Button type="button" color="primary">
                {t('Copy to clipboard')}
              </Button>
            </CopyToClipboard>
          </DialogActions>
        </Page>
      </Dialog>
      <Box flexGrow={1} flexShrink={1} overflow="auto">
        <DialogContent>
          <Grid container spacing={2} mt={3}>
            <Grid item md={6}>
              <FormikInput name="name" label={t('Name')} fullWidth />
            </Grid>
            <Grid item md={6}>
              <FormikInput
                name="code"
                label={t('Code')}
                fullWidth
                inputProps={{ sx: { textTransform: 'uppercase' } }}
              />
            </Grid>
            <Grid item md={6}>
              <FormikInput type="number" name="multiplier" label={t('Multiplier')} fullWidth />
            </Grid>
            <Grid item md={6}>
              <FormikInput select name="level" label={t('Level')} fullWidth>
                <MenuItem value={ProblemLevel.EASY}>Easy</MenuItem>
                <MenuItem value={ProblemLevel.MEDIUM}>Medium</MenuItem>
                <MenuItem value={ProblemLevel.HARD}>Hard</MenuItem>
              </FormikInput>
            </Grid>
            <Grid item md={3}>
              <FormikInput
                type="number"
                name="timeLimit"
                label={t('Time Limit')}
                fullWidth
                InputProps={{
                  endAdornment: 's',
                }}
              />
            </Grid>
            <Grid item md={3}>
              <FormikInput
                type="number"
                name="memoryLimit"
                label={t('Memory Limit')}
                fullWidth
                InputProps={{
                  endAdornment: 'MB',
                }}
              />
            </Grid>
            <Grid item md={12}>
              <FormControlLabel
                control={
                  <Checkbox checked={values.active} onChange={(e) => setFieldValue('active', e.target.checked)} />
                }
                label={t('Active')}
              />
            </Grid>
            <Grid item xs={6}>
              <FormikInput name="description" label={t('Description')} fullWidth multiline rows={7} />
            </Grid>
            <Grid item xs={6}>
              <Paper variant="outlined" sx={{ p: 2, overflow: 'hidden' }}>
                <Box height={160} overflow="auto">
                  <Markdown children={values.description} />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <FieldArray
                name="tests"
                render={(arrayHelper) => (
                  <>
                    <Typography variant="h6">
                      Tests ({values?.tests?.length ?? 0}/{MAX_TESTS})
                      <Tooltip title={t('Add test')}>
                        <IconButton
                          onClick={() =>
                            arrayHelper.push({
                              number: values?.tests?.length ?? 0,
                              active: true,
                            })
                          }
                          disabled={values?.tests?.length >= MAX_TESTS}
                        >
                          <Icon children="add" />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Quick import">
                        <IconButton onClick={onImportFiles}>
                          <Icon children="file_open" />
                        </IconButton>
                      </Tooltip>
                    </Typography>
                    <Typography variant="caption" marginBottom={2}>
                      {t`Note: For import files, file name must be followed pattern testNumber.[in|out]`} <br />
                      Eg: 0.in => Input of test case number 0
                    </Typography>
                    <input
                      ref={fileInputRef}
                      type="file"
                      style={{ display: 'none' }}
                      onChange={onFileChange}
                      accept=".txt,.in,.out"
                      multiple={!fileField}
                    />
                    <TableContainer component={Paper} sx={{ marginTop: 2 }}>
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>UUID</TableCell>
                            <TableCell>Input</TableCell>
                            <TableCell>Output</TableCell>
                            <TableCell>Active</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {values?.tests?.map((test, i) => (
                            <TableRow key={`test-${i}`}>
                              <TableCell>{test.number}</TableCell>
                              <TableCell>{test.uuid ?? 'NEW'}</TableCell>
                              <TableCell>
                                <Tooltip title={t`Upload`}>
                                  <Button
                                    onClick={onUploadFile(`tests[${i}].input`)}
                                    sx={{ textTransform: 'none' }}
                                    variant="outlined"
                                    color={test.input || test.inputUri ? 'primary' : 'error'}
                                  >
                                    <Icon children="upload_file" />
                                    {truncate(test?.input?.name, 5, 5)}
                                  </Button>
                                </Tooltip>

                                {(test.input || test.inputUri) && (
                                  <Tooltip title={test?.input?.name ?? test.inputUri}>
                                    <IconButton onClick={() => onViewFile(test.input || test.inputUri)}>
                                      <Icon children="visibility" />
                                    </IconButton>
                                  </Tooltip>
                                )}
                              </TableCell>
                              <TableCell>
                                <Tooltip title={t`Upload`}>
                                  <Button
                                    onClick={onUploadFile(`tests[${i}].output`)}
                                    sx={{ textTransform: 'none' }}
                                    variant="outlined"
                                    color={test.output || test.outputUri ? 'primary' : 'error'}
                                  >
                                    <Icon children="upload_file" />
                                    {truncate(test?.output?.name, 5, 5)}
                                  </Button>
                                </Tooltip>
                                {(test.output || test.outputUri) && (
                                  <Tooltip title={test?.output?.name ?? test.outputUri}>
                                    <IconButton onClick={() => onViewFile(test.output || test.outputUri)}>
                                      <Icon children="visibility" />
                                    </IconButton>
                                  </Tooltip>
                                )}
                              </TableCell>
                              <TableCell>
                                <Checkbox
                                  checked={test.active}
                                  onChange={(e) => setFieldValue(`tests[${i}].active`, e.target.checked)}
                                />
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </>
                )}
              />
            </Grid>
          </Grid>
        </DialogContent>
      </Box>

      <DialogActions>
        {!isCreate && (
          <>
            <Button type="button" onClick={RemoveDialog.toggle} color="error" variant="outlined">
              <Icon children="delete" />
              {t('Remove')}
            </Button>
            <RemoveDialog.Element message={t`Are you sure you want to remove this problem?`} />
          </>
        )}

        <Button type="button" onClick={close} color="error">
          {t('Cancel')}
        </Button>
        <LoadingButton type="submit" autoFocus loading={loading} variant="contained">
          {t('Save')}
        </LoadingButton>
      </DialogActions>
    </FormikForm>
  );
};
