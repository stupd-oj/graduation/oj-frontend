import { Page } from '@components';
import { LoadingButton } from '@mui/lab';
import { Box, Dialog, DialogContent, DialogTitle, MenuItem, TextField, Typography } from '@mui/material';
import { DataGridPremium } from '@mui/x-data-grid-premium';
import { useFormik } from 'formik';
import { useAppState, useToast } from 'hooks';
import { forwardRef, useEffect, useImperativeHandle, useMemo, useRef, useState } from 'react';
import { CodeService } from 'services';
import { PROGRAM_LANGUAGE } from 'utils/constants';

export const ProblemTestDialog = forwardRef((props, ref) => {
  const [, { showLoading, hideLoading }] = useAppState();
  const [problemId, setProblemId] = useState();
  const [problem, setProblem] = useState();
  const [isOpen, setOpen] = useState(false);
  const { showError } = useToast();
  const waitForRef = useRef();
  const timeoutRef = useRef(0);
  const [tests, setTests] = useState(null);

  const open = async (_problemId) => {
    try {
      setProblemId(_problemId);
      setProblem(null);
      await resetForm();
      await setFieldValue('problemId', _problemId);
      waitForRef.current = null;
      setTests(null);
      setOpen(true);
      const { data: _problem } = await CodeService.adminGetProblemDetail(_problemId);
      setProblem(_problem);
    } catch (e) {
      showError(e.message);
      setOpen(false);
    }
  };

  const fetch = async () => {
    try {
      const { data } = await CodeService.getAdminSubmissionDetail(waitForRef.current);
      console.log(data);
      if (!data.testResults) {
        timeoutRef.current = setTimeout(fetch, 1000);
      } else {
        setTests(data.testResults);
        clearTimeout(timeoutRef.current);
        timeoutRef.current = 0;
        waitForRef.current = null;
      }
    } catch (e) {
      showError(e.message);
    }
  };

  const onSubmit = async (value) => {
    showLoading();
    setTests(null);
    try {
      const { data: submission } = await CodeService.createTestSubmission(value);
      waitForRef.current = submission.id;
      timeoutRef.current = setTimeout(fetch, 1000);
    } catch (e) {
      showError(e.message);
    }
    hideLoading();
  };

  useImperativeHandle(ref, () => ({
    open,
  }));

  const { getFieldProps, setFieldValue, handleSubmit, resetForm } = useFormik({
    initialValues: {
      language: PROGRAM_LANGUAGE.C,
      source: '',
      problemId: '',
    },
    onSubmit,
  });

  const resultRows = useMemo(() => {
    const rows = problem?.tests?.filter(({ active }) => active)?.sort((a, b) => a.number - b.number) ?? [];
    rows.forEach((row) => {
      const rowResult = tests?.find((e) => e.testUuid === row.uuid);
      if (rowResult) {
        row.result = rowResult.result;
        row.time = rowResult.time;
        row.memory = rowResult.memory;
      }
      waitForRef.current = null;
    });
    return rows;
  }, [problem, tests]);

  useEffect(
    () => () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
        timeoutRef.current = null;
      }
    },
    [],
  );

  return (
    <Dialog open={isOpen} onClose={() => setOpen(false)} fullWidth maxWidth="md">
      <Page container={false}>
        <DialogTitle>Submit test problem #{problemId}</DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit}>
            <TextField
              label="Language"
              fullWidth
              sx={{ my: 2 }}
              select
              {...getFieldProps('language')}
              disabled={waitForRef.current}
            >
              {Object.values(PROGRAM_LANGUAGE).map((language) => (
                <MenuItem key={language.name} value={language.name}>
                  {language.name}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              label="Solution"
              multiline
              fullWidth
              rows={10}
              {...getFieldProps('source')}
              disabled={waitForRef.current}
            />
            <Box mt={2}>
              <LoadingButton variant="contained" type="submit" loading={waitForRef.current}>
                Run
              </LoadingButton>
            </Box>
          </form>
          <Box mt={2}>
            <Typography variant="subtitle2" color="primary">
              {waitForRef.current && `#${waitForRef.current}`}
            </Typography>

            <Typography variant="h6">Tests</Typography>
            <DataGridPremium
              columns={COLUMNS}
              rows={resultRows}
              getRowId={({ uuid }) => uuid}
              autoHeight
              hideFooter
              disableColumnMenu
            />
          </Box>
        </DialogContent>
      </Page>
    </Dialog>
  );
});

const COLUMNS = [
  {
    field: 'number',
    headerName: '#',
    flex: 1,
    sortable: false,
  },
  {
    field: 'uuid',
    headerName: 'Uuid',
    flex: 3,
    sortable: false,
  },
  {
    field: 'result',
    headerName: 'Result',
    flex: 1,
    sortable: false,
  },
  {
    field: 'time',
    headerName: 'Time',
    flex: 1,
    sortable: false,
  },
  {
    field: 'memory',
    headerName: 'Memory',
    flex: 1,
    sortable: false,
  },
];
