import { contestValidation } from 'contants/validation';
import { useTranslation } from 'react-i18next';
import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import ROUTES from 'routes/paths';
import { Paper } from '@mui/material';
import { Formik } from 'formik';
import { Page } from '@components';
import { useState } from 'react';
import { useToast } from 'hooks';
import { CodeService, FileService } from 'services';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { ContestForm } from './components/ContestForm';

const CreateContest = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const { showError, showSuccess } = useToast();
  const navigate = useNavigate();

  const initialValues = {
    name: '',
    description: '',
    image: '',
    rule: 'OI',
    startAt: null,
    endAt: null,
    closeRankAt: null,
    password: '',
  };

  const onSubmit = async (payload) => {
    setLoading(true);
    try {
      if (payload.imageFile) {
        payload.image = await FileService.upload(`images/contest/${Date.now()}`, payload.imageFile);
        delete payload.imageFile;
      }
      const { data } = await CodeService.createContest(payload);
      showSuccess(`${t('Create contest successfully')}: ${data}`);
      navigate(ROUTES.ADMIN_CONTEST_LIST);
    } catch (e) {
      showError(e.message);
    }
    setLoading(false);
  };

  const validationSchema = Yup.object().shape({ ...contestValidation });

  return (
    <Page title={t('Contest management')} loading={loading}>
      <HeaderBreadcrumbs
        heading={t('Contest management')}
        links={[
          { name: t('Admin'), href: ROUTES.ADMIN_ROOT },
          { name: t('Contest Management'), href: ROUTES.ADMIN_CONTEST_LIST },
          { name: t('Create Contest') },
        ]}
      />
      <Paper variant="outlined" sx={{ p: 2 }}>
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
          <ContestForm />
        </Formik>
      </Paper>
    </Page>
  );
};

export default CreateContest;
