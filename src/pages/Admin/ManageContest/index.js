import Loadable from '@components/Loadable';
import { lazy } from 'react';

export const ContestList = Loadable(lazy(() => import('./ContestList')));
export const CreateContest = Loadable(lazy(() => import('./CreateContest')));
export const UpdateContest = Loadable(lazy(() => import('./UpdateContest')));
