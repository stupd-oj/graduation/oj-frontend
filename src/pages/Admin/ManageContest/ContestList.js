import { Link } from '@components';
import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import Page from '@components/Page';
import { Box, Icon, IconButton, InputAdornment, Paper, TextField, Tooltip } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import { useEffect } from 'react';
import { ContestColumnDef } from 'contants/columnDefinition';
import { useAuth, useDebounce, usePagination, useToast } from 'hooks';
import ROUTES from 'routes/paths';
import { CodeService } from 'services';
import { USER_ROLES } from 'utils/constants';

const ContestList = () => {
  const [{ filter, data, total }, { setFilterField, getData, setFilter }] = usePagination({
    getDataFunc: CodeService.adminGetContestList,
  });
  const [{ user }, { hasRole }] = useAuth();
  const { showError } = useToast();

  const getDataDebounced = useDebounce(async (_filter) => {
    try {
      const payload = { ..._filter };
      if (!hasRole(USER_ROLES.Admin)) payload.createdById = user.id;

      await getData(payload);
    } catch (e) {
      showError(e.message);
    }
  }, 500);

  useEffect(() => {
    getDataDebounced(filter);
  }, [filter]);

  return (
    <Page title="Contest Management">
      <HeaderBreadcrumbs
        heading="Contest Management"
        links={[
          { name: 'Admin', href: ROUTES.ADMIN_ROOT },
          { name: 'Contest Management', href: ROUTES.ADMIN_CONTEST_LIST },
        ]}
      />
      <Paper variant="outlined" sx={{ px: 2, mt: 3 }}>
        <Toolbar query={filter.query} setQuery={setFilterField('query')} getData={() => getDataDebounced(filter)} />
        <DataGrid
          autoHeight
          disableSelectionOnClick
          sortingMode="server"
          paginationMode="server"
          density="comfortable"
          pagination
          disableColumnMenu
          columns={ContestColumnDef}
          sortModel={filter.sortModel}
          pageSize={filter.pageSize}
          rows={data}
          rowCount={total}
          onPaginationModelChange={(paginationModel) => setFilter({ ...filter, ...paginationModel })}
          onSortModelChange={setFilterField('sortModel')}
          pageSizeOptions={[10, 20, 50]}
        />
      </Paper>
    </Page>
  );
};

export default ContestList;

const Toolbar = ({ query, setQuery, getData }) => (
  <Box sx={{ mt: 2 }} display="flex" alignItems="end" justifyContent="end">
    <Tooltip title="Create contest">
      <Link to={ROUTES.ADMIN_CREATE_CONTEST}>
        <IconButton sx={{ mr: 2 }}>
          <Icon children="add" />
        </IconButton>
      </Link>
    </Tooltip>
    <Tooltip title="Refresh">
      <IconButton onClick={getData} sx={{ mr: 2 }}>
        <Icon children="autorenew" />
      </IconButton>
    </Tooltip>
    <TextField
      type="search"
      value={query}
      onChange={(e) => setQuery(e.target.value)}
      size="small"
      placeholder="Search by name"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Icon children="search" />
          </InputAdornment>
        ),
      }}
    />
  </Box>
);
