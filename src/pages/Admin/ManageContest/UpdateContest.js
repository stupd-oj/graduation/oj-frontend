import { contestValidation } from 'contants/validation';
import { useTranslation } from 'react-i18next';
import HeaderBreadcrumbs from '@components/HeaderBreadcrumbs';
import ROUTES from 'routes/paths';
import { Box, Button, Icon, Paper, Tab } from '@mui/material';
import { Formik } from 'formik';
import Page from '@components/Page';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useConfirmationDialog, useToast } from 'hooks';
import { useNavigate, useParams } from 'react-router-dom';
import { CodeService } from 'services';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import { FileService } from 'services/FileService';
import * as Yup from 'yup';
import { ContestForm } from './components/ContestForm';
import { ProblemList } from './components/ProblemList';
import { ContestantList } from './components/ContestantList';

const UpdateContestPage = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [contest, setContest] = useState(null);
  const [tab, setTab] = useState('update');
  const { contestId } = useParams();
  const { showError, showSuccess } = useToast();
  const navigate = useNavigate();

  useEffect(() => {
    getData(contestId);
  }, [contestId]);

  const getData = async (_id) => {
    setLoading(true);
    try {
      const { data } = await CodeService.adminGetContestDetail(_id);
      setContest(data);
    } catch (e) {
      showError(e.message);
    }
    setLoading(false);
  };

  const onRemove = useCallback(async () => {
    setLoading(true);
    try {
      await CodeService.removeContest(contestId);
      showSuccess('Remove successfully');
      navigate(ROUTES.ADMIN_CONTEST_LIST);
    } catch (e) {
      showError(e.message);
    }
    setLoading(false);
  }, [contestId]);

  const RemoveDialog = useConfirmationDialog(onRemove);

  return (
    <Page title={t('Contest management')} loading={loading}>
      <HeaderBreadcrumbs
        heading={t('Contest management')}
        links={[
          { name: t('Admin'), href: ROUTES.ADMIN_ROOT },
          { name: t('Contest Management'), href: ROUTES.ADMIN_CONTEST_LIST },
          { name: contest?.name },
        ]}
      />
      <Box justifyContent="end" display="flex">
        <Button onClick={RemoveDialog.toggle} color="error" variant="contained">
          <Icon children="delete" />
          Delete
        </Button>
      </Box>
      <TabContext value={tab}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={(e, value) => setTab(value)} variant="fullWidth">
            <Tab disableRipple label="Details" value="update" />
            <Tab disableRipple label="Contestants" value="contestants" />
            <Tab disableRipple label="Problems" value="problems" />
          </TabList>
        </Box>
        <Paper variant="outlined" sx={{ p: 2 }}>
          <TabPanel value="update">
            <UpdateContestDetails setLoading={setLoading} contest={contest} getData={getData} />
          </TabPanel>
          <TabPanel value="contestants">
            <ContestantList contest={contest} />
          </TabPanel>
          <TabPanel value="problems">
            <ProblemList contest={contest} />
          </TabPanel>
        </Paper>
      </TabContext>
      <RemoveDialog.Element message="Are you sure you want to remove this contest?" />
    </Page>
  );
};

const UpdateContestDetails = ({ setLoading, contest, getData }) => {
  const { t } = useTranslation();
  const { showError, showSuccess } = useToast();

  const initialValues = useMemo(
    () => ({
      id: contest?.id,
      name: contest?.name ?? '',
      description: contest?.description ?? '',
      image: contest?.image ?? '',
      rule: contest?.rule ?? 'OI',
      startAt: contest?.startAt ? new Date(contest?.startAt) : null,
      endAt: contest?.endAt ? new Date(contest?.endAt) : null,
      closeRankAt: contest?.closeRankAt ? new Date(contest?.closeRankAt) : null,
      password: contest?.password ?? '',
    }),
    [contest],
  );

  const validationSchema = Yup.object().shape({ name: contestValidation.name });

  const onSubmit = async (payload) => {
    setLoading(true);
    try {
      if (payload.imageFile) {
        payload.image = await FileService.upload(contest.image ?? `images/contest/${contest.id}`, payload.imageFile);
        delete payload.imageFile;
      }
      await CodeService.updateContest(payload?.id, payload);
      showSuccess(`${t('Update contest successfully')}`);
      getData(payload?.id);
    } catch (e) {
      showError(e.message);
    }
    setLoading(false);
  };

  return (
    <Formik initialValues={initialValues} onSubmit={onSubmit} enableReinitialize validationSchema={validationSchema}>
      <ContestForm />
    </Formik>
  );
};

export default UpdateContestPage;
