import { Icon, IconButton, Menu, MenuItem } from '@mui/material';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ROUTES from 'routes/paths';
import { useConfirmationDialog } from 'hooks';

export const ContestAction = ({ row }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const navigate = useNavigate();
  const [removeDialogToggle, removeDialogElement] = useConfirmationDialog(row._remove);

  return (
    <div>
      <IconButton onClick={handleOpen}>
        <Icon children="more_vert" />
      </IconButton>
      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        <MenuItem onClick={() => navigate(ROUTES.ADMIN_UPDATE_CONTEST.replace(':contestId', row.id))}>Edit</MenuItem>
        <MenuItem onClick={removeDialogToggle}>Remove</MenuItem>
      </Menu>
      {removeDialogElement}
    </div>
  );
};
