import { useDebounce, usePagination, useToast } from 'hooks';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { CodeService } from 'services';
import { isNil } from 'lodash';
import { DataGridPremium } from '@mui/x-data-grid-premium';
import { ContestantColumnDef } from 'contants/columnDefinition';
import { Box, Icon, IconButton, InputAdornment, TextField, Tooltip } from '@mui/material';

export const ContestantList = () => {
  const { showError } = useToast();
  const { contestId } = useParams();

  const [{ filter, data, total }, { getData, setFilterField, setFilter }] = usePagination({
    getDataFunc: (params) => CodeService.getListContestant(contestId, params),
    sortFieldMap: (fieldName) => `p.${fieldName}`,
  });

  useEffect(() => {
    if (!isNil(contestId)) {
      getDataDebounced({ ...filter });
    }
  }, [filter, contestId]);

  useEffect(() => {
    getData().catch();
  }, []);

  const getDataDebounced = useDebounce(async (_filter) => {
    try {
      await getData(_filter);
    } catch (e) {
      showError(e.message);
    }
  }, 500);

  return (
    <>
      <Toolbar query={filter.query} setQuery={setFilterField('query')} getData={() => getDataDebounced(filter)} />
      <DataGridPremium
        autoHeight
        disableSelectionOnClick
        sortingMode="server"
        paginationMode="server"
        density="comfortable"
        pagination
        disableColumnMenu
        columns={ContestantColumnDef}
        sortModel={filter.sortModel}
        paginationModel={{ page: filter.page, pageSize: filter.pageSize }}
        rows={data}
        rowCount={total}
        onPaginationModelChange={(paginationModel) => setFilter({ ...filter, ...paginationModel })}
        onSortModelChange={setFilterField('sortModel')}
        pageSizeOptions={[10, 20, 50]}
        getRowId={(row) => row.userId}
      />
    </>
  );
};

const Toolbar = ({ query, setQuery, getData }) => (
  <Box sx={{ mt: 2 }} display="flex" alignItems="end" justifyContent="end">
    <Tooltip title="Refresh">
      <IconButton onClick={getData} sx={{ mr: 2 }}>
        <Icon children="autorenew" />
      </IconButton>
    </Tooltip>
    <TextField
      type="search"
      value={query}
      onChange={(e) => setQuery(e.target.value)}
      size="small"
      placeholder="Search by name"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Icon children="search" />
          </InputAdornment>
        ),
      }}
    />
  </Box>
);
