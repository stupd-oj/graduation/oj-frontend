import { ProblemColumnDef } from 'contants/columnDefinition';
import { ProblemFormDialog } from 'pages/Admin/ManageProblem/components/ProblemFormDialog';
import { ProblemTestDialog } from 'pages/Admin/ManageProblem/components/ProblemTestDialog';
import { useEffect, useRef } from 'react';
import { useDebounce, usePagination, useToast } from 'hooks';
import { useParams } from 'react-router-dom';
import { CodeService } from 'services';
import { isNil } from 'lodash';
import { DataGridPremium } from '@mui/x-data-grid-premium';
import { Box, Icon, IconButton, InputAdornment, TextField, Tooltip } from '@mui/material';

export const ProblemList = ({ contest }) => {
  const { showError } = useToast();
  const { contestId } = useParams();
  const problemDialogRef = useRef();
  const problemTestDialogRef = useRef();

  const [{ filter, data, total }, { getData, setFilterField, setFilter }] = usePagination({
    getDataFunc: CodeService.adminGetProblemList,
    sortFieldMap: (fieldName) => `p.${fieldName}`,
  });

  useEffect(() => {
    if (!isNil(filter.contestId)) {
      getDataDebounced({ ...filter });
    }
  }, [filter]);

  useEffect(() => {
    setFilterField('contestId')(contestId);
  }, [contestId]);

  const getDataDebounced = useDebounce(async (_filter) => {
    try {
      await getData(_filter);
    } catch (e) {
      showError(e.message);
    }
  }, 500);

  return (
    <>
      <Toolbar
        query={filter.query}
        setQuery={setFilterField('query')}
        getData={() => getDataDebounced(filter)}
        onCreate={() => problemDialogRef.current.open()}
      />
      <DataGridPremium
        autoHeight
        disableSelectionOnClick
        sortingMode="server"
        paginationMode="server"
        density="comfortable"
        pagination
        disableColumnMenu
        columns={ProblemColumnDef}
        sortModel={filter.sortModel}
        rows={data.map((e) => ({
          ...e,
          _update: () => problemDialogRef.current.open(e.id),
          _test: () => problemTestDialogRef.current.open(e.id),
        }))}
        pageSize={filter.pageSize}
        rowCount={total}
        onPaginationModelChange={(paginationModel) => setFilter({ ...filter, ...paginationModel })}
        onSortModelChange={setFilterField('sortModel')}
        pageSizeOptions={[10, 20, 50]}
      />
      <ProblemFormDialog contest={contest} ref={problemDialogRef} postSubmit={() => getData(filter)} />
      <ProblemTestDialog ref={problemTestDialogRef} />
    </>
  );
};

const Toolbar = ({ query, setQuery, getData, onCreate }) => (
  <Box sx={{ mt: 2 }} display="flex" alignItems="end" justifyContent="end">
    <Tooltip title="Create problem">
      <IconButton onClick={onCreate} sx={{ mr: 2 }}>
        <Icon children="add" />
      </IconButton>
    </Tooltip>
    <Tooltip title="Refresh">
      <IconButton onClick={getData} sx={{ mr: 2 }}>
        <Icon children="autorenew" />
      </IconButton>
    </Tooltip>
    <TextField
      type="search"
      value={query}
      onChange={(e) => setQuery(e.target.value)}
      size="small"
      placeholder="Search by name"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Icon children="search" />
          </InputAdornment>
        ),
      }}
    />
  </Box>
);
