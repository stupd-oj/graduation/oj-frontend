import { Form, useFormikContext } from 'formik';
import { Box, Button, FormHelperText, Grid, Icon, MenuItem, Paper, Typography } from '@mui/material';
import { FormikDatePicker, FormikInput } from '@components';
import { isNil } from 'lodash';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useDropzone } from 'react-dropzone';
import { useToast } from 'hooks';
import Markdown from '@components/Markdown';

export const ContestForm = () => {
  const { t } = useTranslation();
  const { values, setFieldValue } = useFormikContext();
  const { showError } = useToast();
  const { getRootProps, getInputProps, isDragActive, isDragReject } = useDropzone({
    multiple: false,
    accept: 'image/jpeg, image/png',
    maxSize: 1024 * 1024, // 1 MB
    onDropRejected: ([fileRejection]) => {
      const fileName = fileRejection?.file?.name;
      const message = fileRejection?.errors?.[0]?.message;
      if (fileName && message) {
        showError(`File chosen error: ${fileName} - ${message}`);
      }
    },
    onDrop: ([file]) => {
      if (file) {
        setFieldValue('imageFile', file);
        setFieldValue('image', URL.createObjectURL(file));
      }
    },
  });

  const isEdit = useMemo(() => !isNil(values.id), [values.id]);

  return (
    <Form>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Box display="flex" justifyContent="center">
            <Box
              {...getRootProps()}
              position="relative"
              border="1px dashed"
              height={200}
              width={600}
              maxWidth="100%"
              sx={{
                ...(isDragActive && { opacity: 0.5 }),
                ...(isDragReject && {
                  color: 'error.main',
                  borderColor: 'error.light',
                  bgColor: 'error.lighter',
                }),
              }}
            >
              <input {...getInputProps()} />
              {values.image && (
                <Box
                  component="img"
                  alt="avatar"
                  src={values.image}
                  width="100%"
                  height="100%"
                  sx={{ zIndex: 8, objectFit: 'contain' }}
                />
              )}

              <Box
                display="flex"
                position="absolute"
                alignItems="center"
                justifyContent="center"
                color="white"
                bgcolor="grey.900"
                sx={{
                  opacity: 0,
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  '&:hover': { opacity: 0.72 },
                }}
              >
                <Box component={Icon} children="file_upload" width={24} height={24} />
                <Typography variant="caption">{t('Upload')}</Typography>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={6} />
        <Grid item xs={6}>
          <FormikInput name="name" label={t('Name')} fullWidth />
        </Grid>
        <Grid item xs={6} />
        <Grid item xs={6}>
          <FormikInput name="description" label={t('Description')} fullWidth multiline rows={7} />
        </Grid>
        <Grid item xs={6}>
          <Paper variant="outlined" sx={{ p: 2, overflow: 'hidden' }}>
            <Box height={160} overflow="auto">
              <Markdown children={values.description} />
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <FormikInput name="rule" label={t('Rule')} fullWidth select disabled={isEdit}>
            <MenuItem value="OI">OI</MenuItem>
            <MenuItem value="ACM">ACM</MenuItem>
          </FormikInput>
        </Grid>
        <Grid item xs={3}>
          <FormHelperText>{t('OI - Olympic style, ACM - ACM Style')}</FormHelperText>
        </Grid>
        <Grid item xs={3}>
          <FormikInput name="password" label={t('Password')} fullWidth />
        </Grid>
        <Grid item xs={3}>
          <FormHelperText>{t('Note: Leave password blank will set contest as public')}</FormHelperText>
        </Grid>
        <Grid item xs={3}>
          <FormikDatePicker
            type="datetime"
            name="startAt"
            label={t('Start time')}
            fullWidth
            inputFormat="dd/MM/yyyy HH:mm"
          />
        </Grid>
        <Grid item xs={3}>
          <FormikDatePicker
            type="datetime"
            name="endAt"
            label={t('End time')}
            fullWidth
            inputFormat="dd/MM/yyyy HH:mm"
          />
        </Grid>
        <Grid item xs={3}>
          <FormikDatePicker
            type="datetime"
            name="closeRankAt"
            label={t('Close rank time')}
            inputFormat="dd/MM/yyyy HH:mm"
            fullWidth
          />
        </Grid>
        <Grid item xs={3}>
          <FormHelperText>
            {t('Note: Leave close rank blank will set ranking table realtime always public')}
          </FormHelperText>
        </Grid>

        <Grid item xs={12} display="flex" justifyContent="center">
          <Button variant="text" color="error" sx={{ mr: 2 }} type="reset">
            {t('Reset')}
          </Button>
          <Button variant="contained" type="submit">
            {t('Save')}
          </Button>
        </Grid>
      </Grid>
    </Form>
  );
};
