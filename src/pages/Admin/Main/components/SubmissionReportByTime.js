import { Card, CardContent, CardHeader, colors } from '@mui/material';
import LoadableBox from '@components/LoadableBox';
import { useEffect, useMemo, useState } from 'react';
import { useToast } from 'hooks';
import { StatisticService } from 'services';
import ReactApexChart from 'react-apexcharts';

export function SubmissionReportByTime() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const { showError } = useToast();

  const getData = async () => {
    setLoading(true);
    try {
      const _data = await StatisticService.getSubmissionReportByDate();
      setData(_data);
    } catch (e) {
      showError(e);
    }
    setLoading(false);
  };

  useEffect(() => {
    getData().catch();
  }, []);

  const chartOptions = useMemo(
    () => ({
      stroke: {
        curve: 'smooth',
      },
      xaxis: {
        type: 'datetime',
        labels: {
          format: 'dd/MM/yyyy',
        },
      },
      yaxis: {
        min: 0,
        labels: {
          formatter: (val) => val.toFixed(0),
        },
      },
      colors: [colors.red['300']],
      fill: {
        colors: [`${colors.red['100']}70`],
      },
    }),
    [],
  );

  const chartData = useMemo(
    () =>
      data.map((e) => ({
        x: e.date,
        y: Number(e.count) || 0,
      })),
    [data],
  );

  return (
    <LoadableBox loading={loading}>
      <Card>
        <CardHeader title="Submission by days" />
        <CardContent>
          <ReactApexChart
            height={300}
            series={[
              {
                type: 'area',
                name: 'Submissions',
                data: chartData,
              },
            ]}
            options={chartOptions}
          />
        </CardContent>
      </Card>
    </LoadableBox>
  );
}
