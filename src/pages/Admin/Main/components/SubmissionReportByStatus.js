import { Card, CardContent, CardHeader, colors } from '@mui/material';
import LoadableBox from '@components/LoadableBox';
import { useEffect, useMemo, useState } from 'react';
import { useToast } from 'hooks';
import { StatisticService } from 'services';
import ReactApexChart from 'react-apexcharts';

const COLOR_MAP = {
  CE: colors.red['300'],
  AC: colors.green['500'],
  PAC: colors.green['300'],
  WA: colors.yellow['700'],
  TLE: colors.red['500'],
  RTE: colors.red['700'],
  IE: colors.grey['500'],
  IQ: colors.blue['300'],
  IP: colors.blue['500'],
};

const LABEL_MAP = {
  CE: 'Compile Error',
  AC: 'Accepted',
  PAC: 'Partially Accepted',
  WA: 'Wrong Answer',
  TLE: 'Time Limit Exceeded',
  RTE: 'Runtime Error',
  IE: 'Internal Error',
  IQ: 'In Queue',
  IP: 'In Progress',
};

export function SubmissionReportByStatus() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const { showError } = useToast();

  const getData = async () => {
    setLoading(true);
    try {
      const _data = await StatisticService.getSubmissionReportByStatus();
      setData(_data);
    } catch (e) {
      showError(e);
    }
    setLoading(false);
  };

  useEffect(() => {
    getData().catch();
  }, []);

  const chartOptions = useMemo(
    () => ({
      labels: data.map((e) => LABEL_MAP[e.status] || e.status),
      colors: data.map((e) => COLOR_MAP[e.status] || colors.grey['500']),
    }),
    [data],
  );

  const chartData = useMemo(() => data.map((e) => Number(e.count) || 0, [data]));

  return (
    <LoadableBox loading={loading}>
      <Card>
        <CardHeader title="Submission by statuses" />
        <CardContent>
          <ReactApexChart type="donut" height={300} series={chartData} options={chartOptions} />
        </CardContent>
      </Card>
    </LoadableBox>
  );
}
