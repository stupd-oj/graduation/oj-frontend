import { Card, CardContent, colors, Grid, Stack, Typography } from '@mui/material';
import LoadableBox from '@components/LoadableBox';
import { StatisticService } from 'services';
import { useEffect, useState } from 'react';
import { useToast } from 'hooks';
import { SvgIconStyle } from '@components';
import userIcon from 'assets/images/users.svg';
import contestIcon from 'assets/images/contests.svg';
import Iconify from '@components/Iconify';

const formatNumber = (number) => {
  if (!number || number < 0) return number;
  if (number > 1e6) return `${Math.floor(number / 1e5) / 10}M+`;
  if (number > 1e3) return `${Math.floor(number / 1e2) / 10}K+`;
  return number;
};

export function SystemSummary() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState({
    user: 0,
    contest: 0,
    submission: 0,
  });
  const { showError } = useToast();

  const getData = async () => {
    setLoading(true);
    try {
      const _data = await StatisticService.getSystemSummary();
      setData({
        contest: _data?.contest?.count ?? 0,
        user: _data?.user?.count ?? 0,
        submission: _data?.submission?.count ?? 0,
      });
    } catch (e) {
      showError(e);
    }
    setLoading(false);
  };

  useEffect(() => {
    getData().catch();
  }, []);

  return (
    <>
      <Grid item xs={4}>
        <LoadableBox loading={loading}>
          <Card sx={{ backgroundColor: colors.orange['100'] }}>
            <CardContent>
              <Stack direction="row" justifyContent="space-between">
                <Stack direction="column">
                  <Typography variant="h2" color={colors.orange['900']}>
                    {formatNumber(data.user)}
                  </Typography>
                  <Typography variant="subtitle1" color={colors.orange['900']}>
                    Total Users
                  </Typography>
                </Stack>
                <SvgIconStyle src={userIcon} sx={{ height: 100, width: 100, bgcolor: colors.orange['900'] }} />
              </Stack>
            </CardContent>
          </Card>
        </LoadableBox>
      </Grid>
      <Grid item xs={4}>
        <LoadableBox loading={loading}>
          <Card sx={{ backgroundColor: colors.yellow['100'] }}>
            <CardContent>
              <Stack direction="row" justifyContent="space-between">
                <Stack direction="column">
                  <Typography variant="h2" color={colors.yellow['900']}>
                    {formatNumber(data.contest)}
                  </Typography>
                  <Typography variant="subtitle1" color={colors.yellow['900']}>
                    Total Contests
                  </Typography>
                </Stack>
                <SvgIconStyle src={contestIcon} sx={{ height: 100, width: 100, bgcolor: colors.yellow['900'] }} />
              </Stack>
            </CardContent>
          </Card>
        </LoadableBox>
      </Grid>
      <Grid item xs={4}>
        <LoadableBox loading={loading}>
          <Card sx={{ backgroundColor: colors.blue['100'] }}>
            <CardContent>
              <Stack direction="row" justifyContent="space-between">
                <Stack direction="column">
                  <Typography variant="h2" color={colors.blue['900']}>
                    {formatNumber(data.submission)}
                  </Typography>
                  <Typography variant="subtitle1" color={colors.blue['900']}>
                    Total Submissions
                  </Typography>
                </Stack>
                <Iconify
                  icon="material-symbols:task-outline"
                  sx={{ height: 100, width: 100, color: colors.blue['900'] }}
                />
              </Stack>
            </CardContent>
          </Card>
        </LoadableBox>
      </Grid>
    </>
  );
}
