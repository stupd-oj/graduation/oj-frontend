import { Grid } from '@mui/material';
import Page from '@components/Page';
import { ContestTimeline } from './components/ContestTimeline';
import { SubmissionReportByTime } from './components/SubmissionReportByTime';
import { SubmissionReportByStatus } from './components/SubmissionReportByStatus';
import { SystemSummary } from './components/SystemSummary';

export function AdminMain() {
  return (
    <Page title="Administrator Dashboard">
      <Grid container spacing={3}>
        <SystemSummary />
        <Grid item xs={12} md={8}>
          <SubmissionReportByTime />
        </Grid>
        <Grid item xs={12} md={4}>
          <SubmissionReportByStatus />
        </Grid>
        <Grid item xs={12}>
          <ContestTimeline />
        </Grid>
      </Grid>
    </Page>
  );
}
