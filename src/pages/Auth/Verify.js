import { userValidation } from 'contants/validation';
import { useState, useEffect } from 'react';
import { Box, Container, Stack, TextField, Typography, Button } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { Icon } from '@iconify/react';
import arrowIosBackFill from '@iconify/icons-eva/arrow-ios-back-fill';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Page from '@components/Page';
import ROUTES from 'routes/paths';
import { Form, FormikProvider, useFormik } from 'formik';
import * as Yup from 'yup';
import { LoadingButton } from '@mui/lab';
import { useToast } from 'hooks/useToast';
import { AuthService } from 'services';
import { base64 } from 'utils';
import { useQuery } from 'hooks';

const RootStyle = styled(Page)(({ theme }) => ({
  display: 'flex',
  minHeight: '100%',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(4, 0),
}));

const Verify = () => {
  const { t } = useTranslation();
  const { showSuccess, showError } = useToast();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const [timeToResend, setTimeToResend] = useState(0);
  const token = useQuery('t');
  const { state: locationState } = useLocation();

  useEffect(() => {
    const tokenDecoded = base64.decode(token);
    const [username, code] = tokenDecoded.split('|');
    if (username && code) {
      setValues((v) => ({ ...v, identifier: username, code, fromEmail: true }));
      onSubmit({ identifier: username, code }).catch(console.error);
    }
  }, [token]);

  useEffect(() => {
    if (locationState?.username) {
      setFieldValue('identifier', locationState?.username);
    }
  }, [locationState]);

  const handleSendOtp = async () => {
    const { identifier } = values;
    if (identifier) {
      try {
        await AuthService.sendVerifyOTP({ identifier });
        let timeOut = 60;
        setTimeToResend(() => timeOut);
        const interval = setInterval(() => {
          timeOut -= 1;
          setTimeToResend(() => timeOut);
          if (timeOut === 0) {
            clearInterval(interval);
          }
        }, 1000);
        showSuccess(t('Send OTP successfully!'));
      } catch (e) {
        showError(t(e.message));
      }
    } else {
      showError(t('Username required!'));
    }
  };

  const initialValues = {
    identifier: '',
    code: '',
  };

  const validationSchema = Yup.object().shape({
    identifier: userValidation.username,
    code: userValidation.code,
  });

  const onSubmit = async (payload) => {
    setLoading(true);
    try {
      const { identifier, password, code } = payload;
      await AuthService.verifyAccount({
        identifier,
        password,
        otp: code,
      });
      showSuccess(t('Verify successfully'));
      navigate(ROUTES.LOGIN);
    } catch (e) {
      setFieldValue('fromEmail', false);
      showError(t(e.message));
    }
    setLoading(false);
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  });
  const { errors, touched, handleSubmit, getFieldProps, setValues, values, setFieldValue } = formik;

  return (
    <RootStyle title="Verify Account">
      <Container maxWidth="sm">
        <Button
          size="small"
          component={RouterLink}
          to={ROUTES.LOGIN}
          startIcon={<Icon icon={arrowIosBackFill} width={20} height={20} />}
          mb={3}
        >
          Back to login
        </Button>
        <ContentStyle>
          <Stack direction="column" alignItems="center" sx={{ mb: 5 }}>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="h3" paragraph>
                {t('Please verify your account!')}
              </Typography>
              <Typography sx={{ color: 'text.secondary' }}>
                {t(
                  'We have emailed a 6-digit confirmation code to your email, please enter the code in below box to verify account.',
                )}
              </Typography>
            </Box>
          </Stack>
          <FormikProvider value={formik}>
            <Form autoComplete="off" onSubmit={handleSubmit}>
              <Stack spacing={3}>
                <TextField
                  fullWidth
                  autoComplete="username"
                  type="text"
                  label={t('Username')}
                  {...getFieldProps('identifier')}
                  disabled={values.fromEmail}
                  error={Boolean(touched.identifier && errors.identifier)}
                  helperText={touched.identifier && errors.identifier}
                />
                <TextField
                  fullWidth
                  type="text"
                  label={t('Code')}
                  inputProps={{
                    maxLength: 6,
                  }}
                  {...getFieldProps('code')}
                  disabled={values.fromEmail}
                  error={Boolean(touched.code && errors.code)}
                  helperText={touched.code && errors.code}
                />
              </Stack>
              <LoadingButton fullWidth size="large" type="submit" variant="contained" sx={{ my: 2 }} loading={loading}>
                {t('Verify')}
              </LoadingButton>
              <Button
                fullWidth
                variant="text"
                size="large"
                disabled={timeToResend > 0 || values.fromEmail}
                loading={loading}
                onClick={handleSendOtp}
              >
                {t('Send OTP')} {timeToResend > 0 && `${timeToResend}s`}
              </Button>
            </Form>
          </FormikProvider>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
};

export default Verify;
