import { Box, Container, IconButton, InputAdornment, Link, Stack, Typography, Icon } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Page from '@components/Page';
import ROUTES from 'routes/paths';
import { Form, Formik, useFormikContext } from 'formik';
import { useState } from 'react';
import { LoadingButton } from '@mui/lab';
import APP_CONFIG from '@config';
import logoSingle from 'assets/images/logo-single.png';
import { useToast, useAuth } from 'hooks';
import { AuthService } from 'services';
import FormikInput from '@components/FormikInput';

const LogoImg = styled('img')(() => ({
  width: 200,
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
}));

const FormContent = () => {
  const { isSubmitting } = useFormikContext();
  const { t } = useTranslation();
  const [showPassword, setShowPassword] = useState(false);

  return (
    <Form>
      <Stack spacing={3}>
        <FormikInput
          type="text"
          name="username"
          autoComplete="username"
          fullWidth
          label={t('Username or email address')}
        />

        <FormikInput
          type={showPassword ? 'text' : 'password'}
          name="password"
          fullWidth
          autoComplete="password"
          label={t('Password')}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                  <Icon children={showPassword ? 'visibility_off' : 'visibility'} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Stack>

      <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ my: 2 }}>
        <Link component={RouterLink} variant="subtitle2" to={ROUTES.RESET_PASSWORD} ml="auto">
          {t('Forgot password?')}
        </Link>
      </Stack>

      <LoadingButton fullWidth size="large" type="submit" variant="contained" loading={isSubmitting}>
        {t('Login')}
      </LoadingButton>
    </Form>
  );
};

const Login = () => {
  const [, { saveCredentials }] = useAuth();
  const { t } = useTranslation();
  const { showSuccess, showError } = useToast();
  const navigate = useNavigate();

  const initialValues = {
    username: '',
    password: '',
  };

  const onSubmit = async (values, { setSubmitting }) => {
    try {
      const { data } = await AuthService.login(values);
      saveCredentials(data);
      showSuccess(t('Logged in successfully'));
      navigate(ROUTES.DASHBOARD_ROOT);
    } catch (e) {
      showError(t(e.message));
    }
    setSubmitting(true);
  };

  return (
    <Page title="Login" display="flex" minHeight="100%" alignItems="center" paddingTop={10} paddingBottom={10}>
      <Container maxWidth="sm">
        <Box component="div" maxWidth={480} margin="auto" flexDirection="column" justifyContent="center">
          <Stack direction="row" alignItems="center" sx={{ mb: 5 }}>
            <Box sx={{ flexGrow: 1 }}>
              <LogoImg src={logoSingle} alt="login" />
              <Typography variant="h4" align="center" gutterBottom>
                {`${t('Sign in to')} ${APP_CONFIG.siteName}`}
              </Typography>
            </Box>
          </Stack>
          <Formik initialValues={initialValues} onSubmit={onSubmit}>
            <FormContent />
          </Formik>
          <Stack direction="column" alignItems="center" mt={3}>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="body2" align="center">
                {`${t('Don’t have an account?')}`}
                <Link variant="subtitle2" ml={1} component={RouterLink} to={ROUTES.REGISTER}>
                  {`${t('Register here')} `}
                </Link>
              </Typography>
            </Box>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="body2" align="center">
                <Link variant="subtitle2" component={RouterLink} to={ROUTES.VERIFY_ACCOUNT}>
                  {`${t('Verify account')}`}
                </Link>
              </Typography>
            </Box>
          </Stack>
        </Box>
      </Container>
    </Page>
  );
};

export default Login;
