import { userValidation } from 'contants/validation';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// MUI
import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import { Box, Link, Container, IconButton, InputAdornment, Divider, Stack, TextField, Typography } from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
// Config
import APP_CONFIG from '@config';
import ROUTES from 'routes/paths';
import * as Yup from 'yup';
import { Form, FormikProvider, useFormik } from 'formik';
import { useToast } from 'hooks';
import { AuthService } from 'services';
import Page from '@components/Page';
import logoSingle from 'assets/images/logo-single.png';

const RootStyle = styled(Page)(({ theme }) => ({
  display: 'flex',
  minHeight: '100%',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(4, 0),
}));

const LogoImg = styled('img')(() => ({
  width: 200,
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
}));

export default function Register() {
  const { t } = useTranslation();
  const { showSuccess, showError } = useToast();
  const navigate = useNavigate();
  const initialValues = {
    email: '',
    fullName: '',
    username: '',
    password: '',
    confirmPassword: '',
  };

  const validationSchema = Yup.object().shape({
    email: userValidation.email,
    username: userValidation.username,
    fullName: userValidation.fullName,
    password: userValidation.password,
    confirmPassword: userValidation.confirmPassword,
  });

  const onSubmit = async (values, { setSubmitting }) => {
    try {
      await AuthService.register(values);
      showSuccess(t('Register successfully'));
      navigate(ROUTES.VERIFY_ACCOUNT, { state: { username: values.username } });
    } catch (e) {
      showError(t(e.message));
    } finally {
      setSubmitting(true);
    }
  };

  const formik = useFormik({ initialValues, validationSchema, onSubmit });
  const { errors, touched, isSubmitting, handleSubmit, getFieldProps } = formik;

  const [showPassword, setShowPassword] = useState(false);

  return (
    <RootStyle title="Register">
      <Container>
        <ContentStyle>
          <Stack direction="row" alignItems="center" sx={{ mb: 5 }}>
            <Box sx={{ flexGrow: 1 }}>
              <LogoImg src={logoSingle} alt="login" />
              <Typography variant="h4" align="center" gutterBottom>
                {`${t('Register to')} ${APP_CONFIG.siteName}`}
              </Typography>
            </Box>
          </Stack>
          <FormikProvider value={formik}>
            <Form autoComplete="off" onSubmit={handleSubmit}>
              <Stack spacing={3}>
                <TextField
                  fullWidth
                  autoComplete="email"
                  type="email"
                  label={t('Email address')}
                  {...getFieldProps('email')}
                  error={Boolean(touched.email && errors.email)}
                  helperText={touched.email && errors.email}
                />

                <TextField
                  fullWidth
                  type="text"
                  autoComplete="username"
                  label={t('Username')}
                  {...getFieldProps('username')}
                  error={Boolean(touched.username && errors.username)}
                  helperText={touched.username && errors.username}
                />

                <TextField
                  fullWidth
                  autoComplete="fullName"
                  type="text"
                  label={t('Your full name')}
                  {...getFieldProps('fullName')}
                  error={Boolean(touched.fullName && errors.fullName)}
                  helperText={touched.fullName && errors.fullName}
                />

                <TextField
                  fullWidth
                  autoComplete="password"
                  type={showPassword ? 'text' : 'password'}
                  label={t('Password')}
                  {...getFieldProps('password')}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                          {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  error={Boolean(touched.password && errors.password)}
                  helperText={touched.password && errors.password}
                />
                <TextField
                  fullWidth
                  type={showPassword ? 'text' : 'password'}
                  label={t('Confirm Password')}
                  {...getFieldProps('confirmPassword')}
                  error={Boolean(touched.confirmPassword && errors.confirmPassword)}
                  helperText={touched.confirmPassword && errors.confirmPassword}
                />
              </Stack>
              <LoadingButton
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                sx={{ my: 2 }}
                loading={isSubmitting}
              >
                {t('Register')}
              </LoadingButton>
            </Form>
          </FormikProvider>
          <Divider sx={{ my: 3 }}>OR</Divider>
          <Stack direction="row" alignItems="center" sx={{ mb: 5 }}>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="body2" align="center" sx={{ mt: 3 }}>
                {`${t('Already have an account?')} `}
                <Link variant="subtitle2" component={RouterLink} to={ROUTES.LOGIN}>
                  {`${t('Login')} `}
                </Link>
              </Typography>
            </Box>
          </Stack>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
}
