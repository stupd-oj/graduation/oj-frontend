import { userValidation } from 'contants/validation';
import { useState, useEffect } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useToast } from 'hooks/useToast';
import { useTranslation } from 'react-i18next';
import { styled } from '@mui/material/styles';
import { Box, Container, Typography, TextField, Stack, InputAdornment, IconButton, Button } from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { LoadingButton } from '@mui/lab';
import { AuthService } from 'services';
import { Form, FormikProvider, useFormik } from 'formik';
import * as Yup from 'yup';
import { base64 } from 'utils';
import Page from '@components/Page';
import { useQuery } from 'hooks';
import ROUTES from 'routes/paths';
import { Icon } from '@iconify/react/dist/iconify';
import arrowIosBackFill from '@iconify/icons-eva/arrow-ios-back-fill';
// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  display: 'flex',
  minHeight: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  padding: theme.spacing(12, 0),
}));

// ----------------------------------------------------------------------

export default function ResetPassword() {
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [isOTPSent, setOTPSent] = useState(false);
  const navigate = useNavigate();
  const { showError, showSuccess } = useToast();
  const { t } = useTranslation();
  const [timeToResend, setTimeToResend] = useState(0);
  const token = useQuery('t');

  useEffect(() => {
    const tokenDecoded = base64.decode(token);
    const [username, code] = tokenDecoded.split('|');
    if (username && code) {
      setValues((v) => ({ ...v, identifier: username, code, fromEmail: true }));
      setOTPSent(true);
    }
  }, [token]);

  const handleSendOtp = async () => {
    const { identifier } = values;
    if (identifier) {
      setLoading(true);
      try {
        await AuthService.sendResetPasswordOTP({ identifier });
        setOTPSent(true);
        let timeOut = 60;
        setTimeToResend(() => timeOut);
        const interval = setInterval(() => {
          timeOut -= 1;
          setTimeToResend(() => timeOut);
          if (timeOut === 0) {
            clearInterval(interval);
          }
        }, 1000);
        showSuccess(t('Send OTP successfully!'));
      } catch (e) {
        showError(t(e.message));
      }
      setLoading(false);
    } else {
      showError(t('Username required!'));
    }
  };

  const onSubmit = async (payload) => {
    setLoading(true);
    try {
      const { identifier, password, code } = payload;
      await AuthService.resetPassword({
        identifier,
        password,
        otp: code,
      });
      showSuccess(t('Reset password successfully'));
      navigate(ROUTES.LOGIN);
    } catch (e) {
      setFieldValue('fromEmail', false);
      showError(t(e.message));
    }
    setLoading(false);
  };

  const initialValues = {
    identifier: '',
    password: '',
    code: '',
  };

  const validationSchema = Yup.object().shape({
    identifier: userValidation.username,
    password: userValidation.password,
    code: userValidation.code,
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  });
  const { errors, touched, handleSubmit, getFieldProps, setValues, values, setFieldValue } = formik;

  return (
    <RootStyle title="Reset Password">
      <Container maxWidth="sm">
        <Button
          size="small"
          component={RouterLink}
          to={ROUTES.LOGIN}
          startIcon={<Icon icon={arrowIosBackFill} width={20} height={20} />}
          mb={3}
        >
          Back to login
        </Button>
        <Box maxWidth={480} mx="auto" mt={3}>
          <Typography variant="h3" paragraph>
            {t('Forgot your password?')}
          </Typography>
          <Typography sx={{ color: 'text.secondary', mb: 5 }}>
            {t(
              'Please enter the username associated with your account and We will email you a link to reset your password.',
            )}
          </Typography>
          <FormikProvider value={formik}>
            <Form autoComplete="off" onSubmit={handleSubmit}>
              <Stack spacing={3}>
                <TextField
                  fullWidth
                  autoComplete="username"
                  type="text"
                  label={t('Username')}
                  {...getFieldProps('identifier')}
                  error={Boolean(touched.identifier && errors.identifier)}
                  helperText={touched.identifier && errors.identifier}
                  disabled={isOTPSent}
                  InputProps={{
                    endAdornment: isOTPSent && (
                      <InputAdornment position="end">
                        <Button onClick={handleSendOtp} edge="end" disabled={timeToResend || values.fromEmail}>
                          {timeToResend ? `${timeToResend}s` : t('Resend')}
                        </Button>
                      </InputAdornment>
                    ),
                  }}
                />
                {isOTPSent && (
                  <>
                    <TextField
                      fullWidth
                      type={showPassword ? 'text' : 'password'}
                      label={t('New password')}
                      {...getFieldProps('password')}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                              {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                      error={Boolean(touched.password && errors.password)}
                      helperText={touched.password && errors.password}
                    />
                    <TextField
                      fullWidth
                      type="text"
                      label={t('Code')}
                      inputProps={{
                        maxLength: 6,
                      }}
                      {...getFieldProps('code')}
                      disabled={values.fromEmail}
                      error={Boolean(touched.code && errors.code)}
                      helperText={touched.code && errors.code}
                    />
                  </>
                )}
              </Stack>
              {isOTPSent ? (
                <>
                  <LoadingButton
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                    sx={{ my: 2 }}
                    loading={loading}
                  >
                    {t('Submit')}
                  </LoadingButton>
                </>
              ) : (
                <>
                  <LoadingButton
                    fullWidth
                    variant="contained"
                    size="large"
                    disabled={isOTPSent}
                    sx={{ my: 2 }}
                    loading={loading}
                    onClick={handleSendOtp}
                  >
                    {t('Send Reset Password OTP')}
                  </LoadingButton>
                </>
              )}
            </Form>
          </FormikProvider>
        </Box>
      </Container>
    </RootStyle>
  );
}
