import Page from '@components/Page';
import { AccountAvatar, AccountChangePassword, AccountUpdateForm } from '@components/dashboard/user/account';
import { Box, Container, Stack, Tab, Tabs, Typography, Icon } from '@mui/material';
import { useState } from 'react';

const ACCOUNT_TABS = [
  {
    value: 'Profile',
    icon: <Icon children="ballot" fontSize="small" />,
    component: <AccountUpdateForm />,
  },
  {
    value: 'Update avatar',
    icon: <Icon children="account_circle" fontSize="small" />,
    component: <AccountAvatar />,
  },
  {
    value: 'Change password',
    icon: <Icon children="lock" fontSize="small" />,
    component: <AccountChangePassword />,
  },
];

export default function UserAccount() {
  const [currentTab, setCurrentTab] = useState('Profile');
  return (
    <Page title="Account">
      <Container>
        <Typography variant="h3" sx={{ mb: 3 }}>
          Account
        </Typography>
        <Stack spacing={5}>
          <Tabs
            variant="fullWidth"
            scrollButtons="auto"
            allowScrollButtonsMobile
            value={currentTab}
            onChange={(e, newValue) => setCurrentTab(newValue)}
          >
            {ACCOUNT_TABS.map((tab) => (
              <Tab disableRipple key={tab.value} label={tab.value} icon={tab.icon} value={tab.value} />
            ))}
          </Tabs>
          {ACCOUNT_TABS.map((tab) => {
            const isMatched = tab.value === currentTab;
            return isMatched && <Box key={tab.value}>{tab.component}</Box>;
          })}
        </Stack>
      </Container>
    </Page>
  );
}
