import Page from '@components/Page';
import { Box, Card, Container, Grid, Skeleton, Stack, Typography } from '@mui/material';
import useSettings from 'hooks/useSettings';
import { styled } from '@mui/styles';
import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import UserService from 'services/UserService';
import { ProfileSubmissionInfo, ProfileSolvedInfo, ProfileContestInfo } from '@components/dashboard/user/profile';
import BadgeItem from '@components/BadgeItem';
import ProfileAvatar from '@components/ProfileAvatar';

const CardHeaderProfileStyle = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  position: 'relative',
  alignItems: 'center',
  paddingBottom: '3rem',
});

export default function UserProfile() {
  const { username } = useParams();
  const [profile, setProfile] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { themeStretch } = useSettings();
  const navigate = useNavigate();

  useEffect(async () => {
    try {
      const { data } = await UserService.getPublicProfile(username);
      setProfile(data);
    } catch (err) {
      navigate('/404');
    }
    setIsLoading(false);
  }, [username]);
  return (
    <Page title="User: Profile">
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <Card
          sx={{
            mt: 5,
            minHeight: 200,
            position: 'relative',
            overflow: 'visible',
          }}
        >
          <CardHeaderProfileStyle>
            {!isLoading ? (
              <ProfileAvatar
                avatar={profile.avatar}
                fullName={profile.fullName}
                sx={{
                  mx: 'auto',
                  transform: {
                    xs: 'translateY(-40px)',
                    md: 'translateY(-64px)',
                  },
                  borderWidth: 2,
                  borderStyle: 'solid',
                  borderColor: 'common.white',
                  width: { xs: 80, md: 128 },
                  height: { xs: 80, md: 128 },
                }}
              />
            ) : (
              <Skeleton
                animation="wave"
                variant="circular"
                sx={{
                  mx: 'auto',
                  transform: {
                    xs: 'translateY(-40px)',
                    md: 'translateY(-64px)',
                  },
                  width: { xs: 80, md: 128 },
                  height: { xs: 80, md: 128 },
                }}
              />
            )}
            <Box
              sx={{
                textAlign: 'center',
                width: '100%',
              }}
            >
              {!isLoading ? (
                <>
                  <Typography variant="h5" sx={{ display: 'inline' }}>
                    {profile.fullName}
                  </Typography>{' '}
                  <Typography variant="body1" sx={{ display: 'inline' }}>
                    {`@${profile.username}`}
                  </Typography>
                  <Typography variant="subtitle2">{profile.mood}</Typography>
                  <Stack spacing={1} marginTop={2}>
                    {profile?.contact?.schoolName && (
                      <Stack direction="row" spacing={1} justifyContent="center">
                        <BadgeItem name="School" />
                        <Typography variant="body2">{profile?.contact?.schoolName}</Typography>
                      </Stack>
                    )}
                    {profile?.contact?.className && (
                      <Stack direction="row" spacing={1} justifyContent="center">
                        <BadgeItem name="Class" type="secondary" />
                        <Typography variant="body2">{profile?.contact?.className}</Typography>
                      </Stack>
                    )}
                    {profile?.contact?.studentId && (
                      <Stack direction="row" spacing={1} justifyContent="center">
                        <BadgeItem name="Student ID" type="primary" />
                        <Typography variant="body2">{profile?.contact?.studentId}</Typography>
                      </Stack>
                    )}
                  </Stack>
                </>
              ) : (
                <Skeleton animation="wave" variant="text" width="60%" />
              )}
            </Box>
          </CardHeaderProfileStyle>
        </Card>
        {!isLoading ? (
          <>
            <Grid direction="row" justifyContent="center" alignItems="center" container spacing={2} sx={{ mt: 2 }}>
              <Grid item xs={4}>
                <ProfileSubmissionInfo value={profile.totalSubmissions} />
              </Grid>
              <Grid item xs={4}>
                <ProfileSolvedInfo value={profile.totalSolved} />
              </Grid>
              <Grid item xs={4}>
                <ProfileContestInfo value={profile.totalContests} />
              </Grid>
            </Grid>
          </>
        ) : (
          <Grid direction="row" justifyContent="center" alignItems="center" container spacing={2} sx={{ mt: 2 }}>
            <Grid item xs={4}>
              <Skeleton variant="rectangular" width="100%">
                <ProfileSubmissionInfo />
              </Skeleton>
            </Grid>
            <Grid item xs={4}>
              <Skeleton variant="rectangular" width="100%">
                <ProfileSolvedInfo />
              </Skeleton>
            </Grid>
            <Grid item xs={4}>
              <Skeleton variant="rectangular" width="100%">
                <ProfileContestInfo />
              </Skeleton>
            </Grid>
          </Grid>
        )}
      </Container>
    </Page>
  );
}
