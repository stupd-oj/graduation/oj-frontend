import HttpClient from './HttpClient';
import API_CONFIG from '../routes/apiConfig';

class StatisticService {
  static getContestTimeline() {
    return HttpClient.get(API_CONFIG.GET_CONTEST_TIMELINE).then((res) => res.data);
  }

  static getSystemSummary() {
    return HttpClient.get(API_CONFIG.GET_SYSTEM_SUMMARY).then((res) => res.data);
  }

  static getSubmissionReportByDate() {
    return HttpClient.get(API_CONFIG.GET_SUBMISSION_REPORT_BY_DATE).then((res) => res.data);
  }

  static getSubmissionReportByStatus() {
    return HttpClient.get(API_CONFIG.GET_SUBMISSION_REPORT_BY_STATUS).then((res) => res.data);
  }
}

export default StatisticService;
