import API_CONFIG from 'routes/apiConfig';
import axios from 'axios';
import HttpClient from './HttpClient';

export class FileService {
  static async upload(objectKey, file) {
    const { preSignedUrl, publicUrl } = await this.#getPreSignedUrl(objectKey);
    await axios
      .put(preSignedUrl, file, {
        headers: {
          'Content-Type': file.type ?? 'application/octet-stream',
        },
      })
      .catch(() => {
        throw new Error('Error uploading file');
      });
    return publicUrl;
  }

  static #getPreSignedUrl(objectKey) {
    return HttpClient.post(API_CONFIG.GET_PRE_SIGNED_UPLOAD_URL, { objectKey }).then(({ data }) => data);
  }
}

export default FileService;
