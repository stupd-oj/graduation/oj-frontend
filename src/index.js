// i18n
import 'locales/i18n';
import { LicenseInfo, LocalizationProvider } from '@mui/x-date-pickers-pro';
// scroll bar
import ReactDOM from 'react-dom';
import { StrictMode } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';
// contexts
import { SettingsProvider } from 'contexts/SettingsContext';
import { CollapseDrawerProvider } from 'contexts/CollapseDrawerContext';
//
import { SnackbarProvider } from 'notistack';
import { Provider } from 'react-redux';
import { store, persistor } from '@redux';
import { PersistGate } from 'redux-persist/integration/react';
import { AdapterDateFns } from '@mui/x-date-pickers-pro/AdapterDateFns';
import { AxiosInterceptor } from 'services/HttpClient';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';

// ----------------------------------------------------------------------

ReactDOM.render(
  <StrictMode>
    <HelmetProvider>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <SettingsProvider>
            <CollapseDrawerProvider>
              <SnackbarProvider
                dense
                preventDuplicate
                autoHideDuration={2000}
                maxSnack={5}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
              >
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <BrowserRouter>
                    <AxiosInterceptor>
                      <App />
                    </AxiosInterceptor>
                  </BrowserRouter>
                </LocalizationProvider>
              </SnackbarProvider>
            </CollapseDrawerProvider>
          </SettingsProvider>
        </PersistGate>
      </Provider>
    </HelmetProvider>
  </StrictMode>,
  document.getElementById('root'),
);

serviceWorkerRegistration.unregister();
LicenseInfo.setLicenseKey(
  '391a12471c0e3ea5b376623c943206feTz1GQ0ssRT0yNTI0NjA4MDAwMDAwLFM9cHJlbWl1bSxMTT1wZXJwZXR1YWwsS1Y9Mg==',
);
