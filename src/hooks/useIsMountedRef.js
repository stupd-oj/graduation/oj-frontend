import { useEffect, useRef } from 'react';

/* Avoid set state when unmounted component: */
export default function useIsMountedRef() {
  const isMounted = useRef(true);

  useEffect(
    () => () => {
      isMounted.current = false;
    },
    [],
  );
  return isMounted;
}
