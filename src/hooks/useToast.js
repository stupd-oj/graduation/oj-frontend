import { useSnackbar } from 'notistack';

export const useToast = () => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const showToast = (msg, options) => enqueueSnackbar(msg, options);

  const showSuccess = (msg, options) =>
    enqueueSnackbar(msg, {
      variant: 'success',
      ...options,
    });

  const showError = (e, options) =>
    enqueueSnackbar(e.message ?? e, {
      variant: 'error',
      ...options,
    });

  const showInfo = (msg, options) =>
    enqueueSnackbar(msg, {
      variant: 'info',
      ...options,
    });

  const showWarning = (msg, options) =>
    enqueueSnackbar(msg, {
      variant: 'warning',
      ...options,
    });

  const hideToast = closeSnackbar;

  return {
    showToast,
    showSuccess,
    showError,
    showInfo,
    showWarning,
    hideToast,
  };
};

export default useToast;
