export default function truncate(str = '', left = 0, right = 0) {
  if (str.length <= left + right) {
    return str;
  }
  return `${str.slice(0, left)}...${right ? str.slice(-right) : ''}`;
}
