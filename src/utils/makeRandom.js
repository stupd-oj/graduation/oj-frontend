export default function makeRandom(length = 64) {
  if (window.crypto) {
    return window.crypto.getRandomValues(new Uint32Array(1))[0].toString(36).slice(0, length);
  }
  return Math.random()
    .toString(36)
    .slice(2, length + 2);
}
