export { default as ComingSoonIllustration } from './illustration_coming_soon';
export { default as PageNotFoundIllustration } from './illustration_404';
export { default as IconMedal } from 'assets/icon_medal';
export { default as IconLock } from 'assets/icon_lock';
