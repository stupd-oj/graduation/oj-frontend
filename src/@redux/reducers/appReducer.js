const initialState = {
  loading: false,
};

const actionTypes = {
  SHOW_LOADING: 'SHOW_LOADING',
  HIDE_LOADING: 'HIDE_LOADING',
};

const appReducer = (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case actionTypes.SHOW_LOADING:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.HIDE_LOADING:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

const showLoading = () => ({
  type: actionTypes.SHOW_LOADING,
});

const hideLoading = () => ({ type: actionTypes.HIDE_LOADING });

export default appReducer;
export const actions = { showLoading, hideLoading };
