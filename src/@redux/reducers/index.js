import { combineReducers } from '@reduxjs/toolkit';

import authReducer from './authReducer';
import appReducer from './appReducer';

export default combineReducers({
  authReducer,
  appReducer,
});
