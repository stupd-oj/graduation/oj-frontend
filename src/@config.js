const APP_CONFIG = Object.freeze({
  siteName: process.env.SITE_NAME || 'Stupd Online Judge',
  apiBaseUrl: process.env.API_BASE_URL || 'https://oj-api.stupd.dev/api/v1',
  socketUrl: process.env.SOCKET_URL || 'https://oj-api.stupd.dev/socket.io',
});

export default APP_CONFIG;
