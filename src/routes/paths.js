const ROUTES = Object.freeze({
  /**
   * Main routes
   */
  HOME: '/',
  NOT_FOUND: '/404',
  FORBIDDEN: '/403',

  /**
   * User routes
   */
  USER_ROOT: '/u',
  USER_PROFILE: '/u/:username',
  USER_SETTINGS: '/u/account',

  /**
   * Auth routes
   */
  AUTH_ROOT: '/auth',
  LOGIN: '/auth/login',
  REGISTER: '/auth/register',
  VERIFY_ACCOUNT: '/auth/verify-account',
  RESET_PASSWORD: '/auth/reset-password',

  /**
   * Dashboard routes
   */
  DASHBOARD_ROOT: '/',
  CONTEST_LIST: '/contests',
  CONTEST_DETAIL: '/contests/:contestId',
  CONTEST_LEADERBOARD: '/contests/:contestId/leaderboard',
  CONTEST_PROBLEM_LIST: '/contests/:contestId/problems',
  CONTEST_ANNOUNCEMENT_LIST: '/contests/:contestId/announcements',
  CONTEST_SUBMISSION_LIST: '/contests/:contestId/submissions',

  PROBLEM_LIST: '/problems',
  PROBLEM_DETAIL: '/problems/:problemId',

  SUBMISSION_LIST: '/submissions',

  LEADERBOARD: '/leaderboard',

  /**
   * Admin routes
   */
  ADMIN_ROOT: '/admin',
  ADMIN_CONTEST_LIST: '/admin/contests',
  ADMIN_UPDATE_CONTEST: '/admin/contests/:contestId',
  ADMIN_CREATE_CONTEST: '/admin/contests/create',
  ADMIN_USER_LIST: '/admin/users',
  ADMIN_UPDATE_USER: '/admin/users/:userId',
  ADMIN_FEEDBACK_LIST: '/admin/feedback',
  ADMIN_SITE_SETTINGS: '/admin/settings',
});

export default ROUTES;
