import { lazy } from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import ROUTES from 'routes/paths';
import Loadable from '@components/Loadable';
import AUTH_ROUTES from 'pages/Auth';
import DASHBOARD_ROUTES from 'pages/Dashboard';
import USER_ROUTES from 'pages/User';
import ADMIN_ROUTES from 'pages/Admin';

export default function Router() {
  return useRoutes([
    // Dashboard Routes
    AUTH_ROUTES,
    USER_ROUTES,
    ADMIN_ROUTES,
    DASHBOARD_ROUTES,
    { path: '*', element: <Navigate to="/404" replace /> },
    { path: ROUTES.NOT_FOUND, element: <NotFound /> },
  ]);
}

// Main
const NotFound = Loadable(lazy(() => import('pages/Error/Page404')));
