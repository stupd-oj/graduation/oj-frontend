#FROM node:16-alpine as BUILD
#WORKDIR /app
#COPY ./package.json .
#RUN yarn install
#COPY . .
#RUN yarn run build

FROM nginx:alpine
WORKDIR /data/www
RUN rm -rf ./*
#COPY --from=BUILD /app/build .
COPY ./build/ .
COPY ./nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]
